-- 创建数据库
create database `codeanalysissystem` default character set utf8mb4 collate utf8mb4_0900_ai_ci ;
use codeanalysissystem;
-- 创建表
/*
 Navicat Premium Data Transfer

 Source Server         : mysqlc
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : codeanalysissystem

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 20/04/2022 10:46:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client
-- ----------------------------

create table client
(
	id int auto_increment
		primary key,
	username varchar(50) not null,
	password varchar(50) not null
);

create table filedetail
(
	id int auto_increment
		primary key,
	fileralativepath longtext not null,
	state tinyint not null,
	pid int not null,
	lineCount int not null,
	classCount int not null,
	functionCount int not null,
	variableCount int not null,
	longestClass varchar(100) null,
	longestFunction varchar(100) null,
	classList longtext null,
	functionList longtext null,
	variableList longtext null,
	functionVariable longtext null
);

create index pid
	on filedetail (pid);

create table filegraph
(
	id int auto_increment
		primary key,
	fid int not null,
	state tinyint not null,
	type tinyint not null,
	graphjsonpath varchar(100) default '' not null,
	constraint filegraph_ibfk_1
		foreign key (fid) references filedetail (id)
);

create index fid
	on filegraph (fid);

create table project
(
	id int auto_increment
		primary key,
	projectName varchar(100) not null,
	uploadDate datetime not null,
	userId int not null,
	state tinyint not null,
	version varchar(20) not null,
	notes varchar(255) null,
	lineCount int null,
	fileCount int null,
	classCount int null,
	functionCount int null,
	longestFile longtext null,
	longestClass varchar(100) null,
	longestFunction varchar(100) null,
	projectdirjsonpath longtext null,
	projectPackage longtext null,
	dependency longtext null,
	fileCallRelation longtext null,
	complexFile longtext null,
	projectComplexity varchar(100) null,
	gitlab varchar(300) null,
	language varchar(30) null,
	maxComplexity varchar(100) null,
	mostComplexFile longtext null,
	constraint project_ibfk_1
		foreign key (userId) references client (id)
);

create table localproject
(
	id int auto_increment
		primary key,
	pid int not null,
	path longtext null,
	savedtime datetime not null,
	constraint localproject_ibfk_1
		foreign key (pid) references project (id)
);

create index userId
	on project (userId);

create table projectgraph
(
	id int auto_increment
		primary key,
	pid int not null,
	state tinyint not null,
	type tinyint not null,
	graphjsonpath varchar(100) default '' not null,
	constraint projectgraph_ibfk_1
		foreign key (pid) references project (id)
);

create index pid
	on projectgraph (pid);




