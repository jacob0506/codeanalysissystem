package com.hlc.codeanalyzesystem.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ProjectGraphDao {

   int deleteByPid(Integer pid);

   int updateByPid(@Param("pid") Integer pid);
}
