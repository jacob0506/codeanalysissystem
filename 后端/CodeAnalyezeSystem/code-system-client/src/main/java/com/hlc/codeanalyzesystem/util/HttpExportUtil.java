package com.hlc.codeanalyzesystem.util;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class HttpExportUtil {
    public static void sendFile(HttpServletResponse response, ResponseEntity<byte[]> responseNew) throws Exception {
        //获取请求SSM系统返回报文中文件字节码
        byte[] result = responseNew.getBody();

        //将数据回写到SpringCloud系统返回参数
        response.setHeader("content-type", String.valueOf(responseNew.getHeaders().getContentType()));
        response.setHeader("Content-Disposition", String.valueOf(responseNew.getHeaders().getContentDisposition()));
        //response.setHeader("Accept-Ranges", "bytes");
        response.setContentType(String.valueOf(responseNew.getHeaders().getContentType()));
        try (InputStream inputStream = new ByteArrayInputStream(result); OutputStream outputStream = response.getOutputStream()) {
            byte[] buff = new byte[1024];
            // 读取字节流
            int read = inputStream.read(buff);
            //通过while循环写入到指定了的对象中
            while (read != -1) {
                String temp = new String(buff, StandardCharsets.UTF_8);
                outputStream.write(buff, 0, buff.length);
                //outputStream.flush();
                read = inputStream.read(buff);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("send file Exception");
        }
    }

    public static void sendFile2(HttpServletResponse response, ResponseEntity<byte[]> responseNew) throws Exception {
        //获取请求SSM系统返回报文中文件字节码
        byte[] result = responseNew.getBody();

        //将数据回写到SpringCloud系统返回参数
        response.setHeader("content-type", String.valueOf(responseNew.getHeaders().getContentType()));
        response.setHeader("Content-Disposition", String.valueOf(responseNew.getHeaders().getContentDisposition()));
        response.setContentType(String.valueOf(responseNew.getHeaders().getContentType()));
        try (InputStream inputStream = new ByteArrayInputStream(result); OutputStream outputStream = response.getOutputStream()) {
            byte[] buff = new byte[1024];
            // 读取字节流
            //int read = inputStream.read(buff);
            int length;
            //通过while循环写入到指定了的对象中
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0,length);
                outputStream.flush();
                //read = inputStream.read(buff);
                //System.out.println(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("send file Exception");
        }
    }

}
