package com.hlc.codeanalyzesystem.controller;



import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.util.HttpExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@CrossOrigin
public class ReverseEngineeringController {

    public static final String PAYMENT_URL = "http://CODE-REVERSE-ENGINEERING";
    @Resource
    private RestTemplate restTemplate;

    //类结构分析
    @RequestMapping("/classStructure/analysis/{pid}")
    public Object cdAnalysis(@PathVariable("pid") Integer pid, @RequestParam("userId") Integer userId){
        log.info("client-- classDiagram");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        return restTemplate.getForObject(PAYMENT_URL+"/classStructure/analysis/" + pid + "?userId={userId}", ResultJSON.class,params);
    }

    //导出类结构
    @RequestMapping("/classStructure/export/{pid}")
    public void projectDependencyGraphExport(@PathVariable("pid") Integer pid, @RequestParam("userId") Integer userId, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/classStructure/export/" + pid + "?userId=" + userId;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume ClassStructureExport=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("ClassStructureExport Exception");
        }
    }

    //控制流分析
    @RequestMapping("/controlFlow/analysis/{pid}")
    public Object cfgAnalysis(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId){
        log.info("client-- controlFLowGraph");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        return restTemplate.getForObject(PAYMENT_URL+"/controlFlow/analysis/" + pid + "?userId={userId}&fileName={fileName}", ResultJSON.class,params);
    }

    //导出控制流
    @RequestMapping("/controlFlow/export/{pid}")
    public void cfgExport(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/controlFlow/export/" + pid + "?userId=" + userId + "&fileName="+ fileName ;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume ControlFlowExport=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("ControlFlowExport Exception");
        }
    }

    //分析数据依赖
    @RequestMapping("/dataDependence/analysis/{pid}")
    public Object ddgAnalysis(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId,@RequestParam("withCtrl") boolean withCtrl){
        log.info("client-- dataDependenceGraph");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        params.put("withCtrl", withCtrl);
        return restTemplate.getForObject(PAYMENT_URL+"/dataDependence/analysis/" + pid + "?userId={userId}&fileName={fileName}&withCtrl={withCtrl}", ResultJSON.class,params);
    }

    //导出数据依赖
    @RequestMapping("/dataDependence/export/{pid}")
    public void ddgExport(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId,@RequestParam("withCtrl") boolean withCtrl, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/dataDependence/export/" + pid + "?userId=" + userId + "&fileName=" + fileName + "&withCtrl=" + withCtrl ;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume DataDependenceExport=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("DataDependenceExport Exception");
        }
    }
    //分析项目目录
    @RequestMapping("/projectDir/{pid}")
    public Object analyzeProjectDir(@PathVariable("pid") Integer pid){
        log.info("client-- projectDir");
        //Map<String,Object> params = new HashMap<>();
        return restTemplate.getForObject(PAYMENT_URL+"/projectDir/" + pid,ResultJSON.class);
    }

}
