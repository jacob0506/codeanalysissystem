package com.hlc.codeanalyzesystem.service;


import com.hlc.codeanalyzesystem.config.Path;
import com.hlc.codeanalyzesystem.dao.*;
import com.hlc.codeanalyzesystem.entities.*;
import com.hlc.codeanalyzesystem.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService {

    //public static final String PROJECT_DIR = "/home/resource/";
    public static final String PROJECT_DIR = Path.Project_dir;

    @Autowired
    ProjectDao projectDao;

    @Autowired
    RecordDao recordDao;

    @Autowired
    ProjectGraphDao projectGraphDao;

    @Autowired
    FileGraphDao fileGraphDao;

    @Autowired
    LocalProjectDao localProjectDao;

    @Autowired
    FiledetailDao filedetailDao;

    public List<Project> queryProjectByUserId(Integer userId){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        return projectList;
    }

    public String queryProjectNameByPid(Integer proId){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andIdEqualTo(proId);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        if(projectList.isEmpty()) return "";
        return projectList.get(0).getProjectname();
    }

    public List<Project> queryProjectByUserIdAndPageNo(Integer userId,Integer pageNo){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        projectExample.setLeftLimit((pageNo - 1) * 8);
        projectExample.setLimitSize(8);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        return projectList;
    }

    public List<Project> queryProjectByNameAndPageNo(Integer userId,String query){
        /*ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        projectExample.setLeftLimit((pageNo - 1) * 8);
        projectExample.setLimitSize(8);*/
        String q = "'"+query+"'";
        if(q.length()>3) {
            List<Project> projectList = projectDao.queryByName(userId,q);
            for(Project project:projectList){
            System.out.println(project.getProjectname());
        }
            return projectList;
        }

        return null;
    }

    public long queryProjectCountByUserId(Integer userId){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        long cnt = projectDao.countByExample(projectExample);
        return cnt % 8 == 0 ? cnt/8 : cnt/8 + 1;
    }


    public int insertProject(String projectName,Integer userId,String notes, String version, String language){
        Project project = new Project();
        project.setProjectname(projectName);
        project.setUploaddate(new Date());
        project.setUserid(userId);
        project.setNotes(notes);
        project.setVersion(version);
        project.setState(0);
        project.setLanguage(language);
        projectDao.insertSelective(project);
        Integer id = project.getId();

        return id;
    }

    public int insertProjectByGitlab(String projectName,Integer userId,String notes, String version, String language,String gitlab){
        Project project = new Project();
        project.setProjectname(projectName);
        project.setUploaddate(new Date());
        project.setUserid(userId);
        project.setNotes(notes);
        project.setVersion(version);
        project.setState(0);
        project.setLanguage(language);
        project.setGitlab(gitlab);
        projectDao.insertSelective(project);
        Integer id = project.getId();
        return id;
    }

    public static boolean deleteFiles(String dir) throws Exception {
        File dirFile = new File(dir);
        if ((!dirFile.exists()) || (!dirFile.isDirectory())) {
            System.out.println("删除文件夹失败：" + dir + "不存在！");
            return false;
        }
        boolean flag = true;
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            // 删除子文件
            if (files[i].isFile()) {
                File file = new File(files[i].getAbsolutePath());
                if(file.exists()&&file.isFile()){
                    if(file.delete()){
                        flag = true;
                    }else {
                        flag = false;
                    }
                }
                if (!flag)
                    break;
            }
            // 删除子文件夹
            else if (files[i].isDirectory()) {
                flag = ProjectService.deleteFiles(files[i].getAbsolutePath());
                if (!flag)
                    break;
            }
        }
        if (!flag) {
            System.out.println("删除文件夹失败！");
            return false;
        }
        // 删除当前文件夹
        if (dirFile.delete()) {
            System.out.println("删除文件夹" + dir + "成功！");
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteProject(Integer id) throws Exception{
        String dir = PROJECT_DIR + id + File.separator;
        deleteFiles(dir);
        localProjectDao.deleteByPid(id);
        projectGraphDao.deleteByPid(id);
        projectDao.deleteByPrimaryKey(id);
        fileGraphDao.deleteByPid(id);
        filedetailDao.deleteByPid(id);
        if(deleteFiles(dir)&&projectDao.deleteByPrimaryKey(id)==1&&projectGraphDao.deleteByPid(id)==1&&fileGraphDao.deleteByPid(id)==1){
            return true;
        }else return false;
    }

    public int updateProject(MultipartFile[] files, String notes,String version,Integer id) throws Exception{
        Project project = new Project();
        project.setId(id);
        project.setVersion(version);
        project.setNotes(notes);
        project.setState(1);
        project.setProjectdirjsonpath("");
        int res = projectDao.updateByPrimaryKeySelective(project);
        filedetailDao.updateByPid(id);
        projectGraphDao.updateByPid(id);
        fileGraphDao.updateByPid(id);
        String dir = PROJECT_DIR + id + File.separator;
        deleteFiles(dir);
        String fileName="";
        String filePath="";
        File file;
        for (MultipartFile f : files) {
            //todo 这里可能存在问题，不同浏览器这个获取结果不一样
            fileName=f.getOriginalFilename();

            filePath=PROJECT_DIR + id + File.separator + fileName.substring(0,fileName.lastIndexOf("/"));
            if(!FileUtil.isDir(filePath)){
                FileUtil.makeDirs(filePath);
            }
            file = new File(PROJECT_DIR + id + File.separator + fileName);
            file.createNewFile();
            f.transferTo(file);
        }
        return res;
    }

    public int updateProjectByGitlab(String notes,String version,Integer id, String gitlab) throws Exception{
        Project project = new Project();
        project.setId(id);
        project.setVersion(version);
        project.setNotes(notes);
        project.setState(1);
        project.setGitlab(gitlab);
        project.setProjectdirjsonpath("");
        int res = projectDao.updateByPrimaryKeySelective(project);
        filedetailDao.updateByPid(id);
        projectGraphDao.updateByPid(id);
        fileGraphDao.updateByPid(id);
        String dir = PROJECT_DIR + id + File.separator;
        deleteFiles(dir);
        localProjectDao.deleteByPid(id);
        /*String fileName="";
        String filePath="";
        File file;
        for (MultipartFile f : files) {
            //todo 这里可能存在问题，不同浏览器这个获取结果不一样
            fileName=f.getOriginalFilename();

            filePath=PROJECT_DIR + id + File.separator + fileName.substring(0,fileName.lastIndexOf("/"));
            if(!FileUtil.isDir(filePath)){
                FileUtil.makeDirs(filePath);
            }
            file = new File(PROJECT_DIR + id + File.separator + fileName);
            file.createNewFile();
            f.transferTo(file);
        }*/
        return res;
    }

    @Transactional(rollbackFor = Exception.class)
    public int saveProject(MultipartFile[] dir,String projectName,Integer userId,String notes, String version,String language) throws Exception {
        File file;
        String fileName="";
        String filePath="";
        Integer pid = insertProject(projectName,userId,notes,version,language);
        if(pid == null){
            throw new Exception("insert exception");
        }

        for (MultipartFile f : dir) {
            //todo 这里可能存在问题，不同浏览器这个获取结果不一样
            fileName=f.getOriginalFilename();
            
            filePath=PROJECT_DIR + pid + File.separator + fileName.substring(0,fileName.lastIndexOf("/"));
            if(!FileUtil.isDir(filePath)){
                FileUtil.makeDirs(filePath);
            }
            file = new File(PROJECT_DIR + pid + File.separator + fileName);
            file.createNewFile();
            f.transferTo(file);
        }
        String project_dir = PROJECT_DIR + pid + File.separator;
        Localproject localproject = new Localproject();
        localproject.setPid(pid);
        localproject.setPath(project_dir);
        localproject.setSavedtime(new Date());
        localProjectDao.insertSelective(localproject);
        return pid;
    }

    @Transactional(rollbackFor = Exception.class)
    public int saveProjectByGitlab(String projectName,Integer userId,String notes, String version,String language,String gitlab) throws Exception {
        Integer pid = insertProjectByGitlab(projectName,userId,notes,version,language,gitlab);
        if(pid == null){
            throw new Exception("insert exception");
        }
        return pid;
    }
    
}
