package com.hlc.codeanalyzesystem.service;

import com.hlc.codeanalyzesystem.dao.ClientDao;
import com.hlc.codeanalyzesystem.entities.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientDao clientDao;

    public Client selectByUsername(String username){
        return clientDao.selectByUsername(username);
    }

    public Client selectById(Integer id){
        return clientDao.selectByPrimaryKey(id);
    }

    public int insertClient(String userName,String password){
        Client client1 = clientDao.selectByUsername(userName);
        if(client1 != null) return -1;
        Client client = new Client();
        client.setUsername(userName);
        client.setPassword(password);
        int res = clientDao.insertSelective(client);
        return res;
    }
}
