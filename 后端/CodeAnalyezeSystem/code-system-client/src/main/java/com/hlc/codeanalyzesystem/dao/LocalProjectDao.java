package com.hlc.codeanalyzesystem.dao;

import com.hlc.codeanalyzesystem.entities.Localproject;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LocalProjectDao {

    Localproject selectByPrimaryKey(Integer id);

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Localproject localproject);

    int deleteByPid(Integer pid);

}
