package com.hlc.codeanalyzesystem.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface FileGraphDao {
    int deleteByPid(@Param("pid") Integer pid);

    int updateByPid(@Param("pid") Integer pid);
}
