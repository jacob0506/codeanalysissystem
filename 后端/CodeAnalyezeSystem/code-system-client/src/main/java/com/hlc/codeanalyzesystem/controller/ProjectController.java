package com.hlc.codeanalyzesystem.controller;

import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/myProject")
public class ProjectController {

    public static final String REV_URL = "http://CODE-REVERSE-ENGINEERING";
    public static final String STRU_URL = "http://CODE-STRUCTURE-ANALYZE";

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value="/list",method= RequestMethod.GET)
    public List<Project> projectList(@RequestParam("userId") Integer userId,@RequestParam(value = "pageNo",required = false) Integer pageNo) {
        if(userId == null) {
            log.info("userId could not be null");
            return null;
        }
        if(pageNo == null){
            pageNo = 1;
        }
        List<Project> projectList = projectService.queryProjectByUserIdAndPageNo(userId,pageNo);
        /*for(Project project : projectList){
            System.out.println(project.getNotes());
        }*/
        return projectList;
    }

    @RequestMapping(value="/SearchList",method= RequestMethod.GET)
    public List<Project> SearchList(@RequestParam("userId") Integer userId,
                                    @RequestParam("query") String query) {
        if(query == null) {
            log.info("projectName could not be null");
            return null;
        }
        List<Project> projectList = projectService.queryProjectByNameAndPageNo(userId,query);
        return projectList;
    }

    @RequestMapping(value="/listCount",method= RequestMethod.GET)
    public Long projectListCount(@RequestParam("userId") Integer userId) {
        if(userId == null) {
            log.info("userId could not be null");
            return null;
        }
        Long projectCount = projectService.queryProjectCountByUserId(userId);
        return projectCount;
    }


    @PostMapping(value="/upload/file")
    public void upload(@RequestParam("files") MultipartFile[] dir,
                       @RequestParam("userDefineName") String projectName,
                       @RequestParam("id") Integer userId,
                       @RequestParam("notes") String notes,
                       @RequestParam("version") String version,
                       @RequestParam("language") String language) {
        try {
            log.info("project upload start");
            int projectId = projectService.saveProject(dir,projectName,userId,notes,version,language);
            //以下函数用于在各模块机子上同步文件，如果单机部署多模块，无需开启
            // transferByRest(dir,projectId);
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project upload Exception");
        }
    }

    @PostMapping(value="/upload/gitlab")
    public void uploadByGitlab(@RequestParam("userDefineName") String projectName,
                       @RequestParam("id") Integer userId,
                       @RequestParam("notes") String notes,
                       @RequestParam("version") String version,
                       @RequestParam("language") String language,
                       @RequestParam("gitlab") String gitlab) {
        try {
            log.info("project upload start");
            int projectId = projectService.saveProjectByGitlab(projectName,userId,notes,version,language,gitlab);
            //以下函数用于在各模块机子上同步文件，如果单机部署多模块，无需开启
            // transferByRest(dir,projectId);
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project upload Exception");
        }
    }


    @PostMapping(value="/update/gitlab")
    public int updateGitlab(@RequestParam("id") Integer id,
                       @RequestParam("notes") String notes,
                       @RequestParam("version") String version,
                            @RequestParam("gitlab") String gitlab) {
        try {
            log.info("project update start");
            System.out.println(gitlab);
            int res = projectService.updateProjectByGitlab(notes,version,id,gitlab);
            return 0;
            //以下函数用于在各模块机子上同步文件，如果单机部署多模块，无需开启
            // transferByRest(dir,projectId);
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project update Exception");
            return -1;
        }
    }

    @PostMapping(value="/update/file")
    public void update(@RequestParam("id") Integer id,
                      @RequestParam("files") MultipartFile[] dir,
                      @RequestParam("notes") String notes,
                      @RequestParam("version") String version) {
        try {
            log.info("project update start");
            int res = projectService.updateProject(dir,notes,version,id);
            //以下函数用于在各模块机子上同步文件，如果单机部署多模块，无需开启
            // transferByRest(dir,projectId);
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project update Exception");
        }
    }

    @RequestMapping(value="/delete/{pid}",method= RequestMethod.GET)
    public int deleteProject(@PathVariable("pid") Integer pid){
        try {
            log.info("delete project");
            boolean res = projectService.deleteProject(pid);
            return 0;
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project delete Exception");
            return -1;
        }
    }

    public void transferByRest(MultipartFile[] file,Integer projectId)throws IOException {
        RestTemplate restTemplate=new RestTemplate();
        //设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        //设置请求体，注意是LinkedMultiValueMap
        List<Object>fileList=new ArrayList<>();
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        for(MultipartFile multipartFile : file) {
            ByteArrayResource byteArrayResource = new ByteArrayResource(multipartFile.getBytes()){
                @Override
                public String getFilename() throws IllegalStateException {
                    return multipartFile.getOriginalFilename();
                }
            };
            fileList.add(byteArrayResource);
        }
        form.put("files", fileList);
        form.add("id",projectId);
        //用HttpEntity封装整个请求报文
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(form, headers);
        restTemplate.postForObject(REV_URL + "/upload",entity,String.class);
        restTemplate.postForObject(STRU_URL + "/upload",entity,String.class);
        return ;
    }
}
