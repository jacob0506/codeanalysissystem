package com.hlc.codeanalyzesystem.controller;

import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.entity.Structure.FileDependencyVo;
import com.hlc.codeanalyzesystem.entity.Structure.FileResultVo;
import com.hlc.codeanalyzesystem.entity.Structure.G6.G6Graph;
import com.hlc.codeanalyzesystem.entity.Structure.ProjectResultVo;
import com.hlc.codeanalyzesystem.entity.Structure.TreeNodeVo;
import com.hlc.codeanalyzesystem.util.HttpExportUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@CrossOrigin
public class StructureAnalyzeController {

    public static final String PAYMENT_URL = "http://CODE-STRUCTURE-ANALYZE";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/projectAnalyze/initial/{id}")
    public Object projectInitialAnalyze(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId){
        log.info("client--projectInitial");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        return restTemplate.getForObject(PAYMENT_URL+"/projectAnalyze/initial/" + id + "?userId={userId}", ResultJSON.class,params);
    }

    //项目依赖图
    @RequestMapping("/projectAnalyze/dependency/{id}")
    public Object projectDependencyGraph(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId){
        log.info("client--projectDependency");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        return restTemplate.getForObject(PAYMENT_URL+"/projectAnalyze/dependency/" + id + "?userId={userId}", G6Graph.class,params);
    }

    //项目依赖图
    @RequestMapping("/projectAnalyze/call/{id}")
    public Object projectcallGraph(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId){
        log.info("client--projectCall");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        return restTemplate.getForObject(PAYMENT_URL+"/projectAnalyze/call/" + id + "?userId={userId}", G6Graph.class,params);
    }

    //文件概况
    @RequestMapping("/fileAnalyze/initial/{pid}")
    public Object fileInitialAnalyze(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId){
        log.info("client--fileInitial");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        return restTemplate.getForObject(PAYMENT_URL+"/fileAnalyze/initial/" + pid + "?userId={userId}&fileName={fileName}", FileResultVo.class,params);
    }

    @RequestMapping("/fileAnalyze/dependency/{pid}")
    public Object fileDependencyGraph(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId){
        log.info("client--file dependency");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        return restTemplate.getForObject(PAYMENT_URL+"/fileAnalyze/dependency/" + pid + "?userId={userId}&fileName={fileName}", FileDependencyVo.class,params);

    }

    //文件调用图
    @RequestMapping("/fileAnalyze/call/{pid}")
    public Object fileCallGraph(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId){
        log.info("client--file call");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        return restTemplate.getForObject(PAYMENT_URL+"/fileAnalyze/call/" + pid + "?userId={userId}&fileName={fileName}", FileDependencyVo.class,params);
    }

    //文件抽象语法树
    @RequestMapping("/fileAnalyze/ast/{pid}")
    public Object fileAST(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId){
        log.info("client--file ast");
        Map<String,Object> params = new HashMap<>();
        params.put("userId",userId);
        params.put("fileName", fileName);
        return restTemplate.getForObject(PAYMENT_URL+"/fileAnalyze/ast/" + pid + "?userId={userId}&fileName={fileName}",List.class,params);
    }

    @RequestMapping("/projectAnalyze/dependency/export/{id}")
    public void projectDependencyGraphExport(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/projectAnalyze/dependency/export/" + id + "?userId=" + userId;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume Dependency Export=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("DependencyGraphExport Exception");
        }
    }

    @RequestMapping("/projectAnalyze/call/export/{id}")
    public void projectCallGraphExport(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId, HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/projectAnalyze/call/export/" + id + "?userId=" + userId;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume Call Export=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("CallGraphExport Exception");
        }
    }


    //导出抽象语法树
    @RequestMapping("/fileAnalyze/ast/export/{pid}")
    public void exportFileAST(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName,@RequestParam("userId") Integer userId,HttpServletResponse response){
        HttpHeaders headers = new HttpHeaders();
        String url = PAYMENT_URL + "/fileAnalyze/ast/export/" + pid + "?fileName="+ fileName +"&userId=" + userId;

        //请求系统获取要下载的文件流
        ResponseEntity<byte[]> responseNew = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<byte[]>(headers), byte[].class);
        log.info("=================consume ast Export=================");
        // 下载文件
        try {
            HttpExportUtil.sendFile2(response, responseNew);
        }
        catch (Exception e){
            log.info("astExport Exception");
        }
    }

}
