package com.hlc.codeanalyzesystem.controller;


import com.hlc.codeanalyzesystem.utils.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
@Slf4j
public class ProjectUploadController {

    public static final String PROJECT_DIR = "/Users/katerina/Desktop/codesystem/CodeSystem/resources/";

    @PostMapping(value="/upload")
    public void upload(@RequestParam("files") MultipartFile[] dir, @RequestParam("id") Integer pid) {
        try {
            log.info("project upload start");
            saveProject(dir,pid);
        }
        catch (Exception e) {
            e.printStackTrace();
            log.info("project upload Exception");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public int saveProject(MultipartFile[] dir,Integer pid) throws Exception {
        File file;
        String fileName="";
        String filePath="";
        if(pid == null){
            throw new Exception("insert exception");
        }

        for (MultipartFile f : dir) {
            fileName=f.getOriginalFilename();

            filePath=PROJECT_DIR + pid + "/" + fileName.substring(0,fileName.lastIndexOf("/"));
            if(!FileUtil.isDir(filePath)){
                FileUtil.makeDirs(filePath);
            }
            file = new File(PROJECT_DIR + pid + "/" + fileName);
            file.createNewFile();
            f.transferTo(file);
        }
        return pid;
    }
}
