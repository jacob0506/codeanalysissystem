package com.hlc.codeanalyzesystem.entity.CD;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CD_Parameter {
    private String name;
    private String type;
}
