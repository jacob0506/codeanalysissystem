package com.hlc.codeanalyzesystem.entity.CFG;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CF_Node {
    private Integer key;
    private Integer line;
    private String label;
}
