package com.hlc.codeanalyzesystem.entity;

public class FileresultReWithBLOBs extends FileresultRe {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column fileresult_re.cfg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    private String cfgJson;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column fileresult_re.ddgWithCtrl_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    private String ddgwithctrlJson;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column fileresult_re.ddg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    private String ddgJson;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fileresult_re.cfg_json
     *
     * @return the value of fileresult_re.cfg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public String getCfgJson() {
        return cfgJson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fileresult_re.cfg_json
     *
     * @param cfgJson the value for fileresult_re.cfg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public void setCfgJson(String cfgJson) {
        this.cfgJson = cfgJson == null ? null : cfgJson.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fileresult_re.ddgWithCtrl_json
     *
     * @return the value of fileresult_re.ddgWithCtrl_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public String getDdgwithctrlJson() {
        return ddgwithctrlJson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fileresult_re.ddgWithCtrl_json
     *
     * @param ddgwithctrlJson the value for fileresult_re.ddgWithCtrl_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public void setDdgwithctrlJson(String ddgwithctrlJson) {
        this.ddgwithctrlJson = ddgwithctrlJson == null ? null : ddgwithctrlJson.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column fileresult_re.ddg_json
     *
     * @return the value of fileresult_re.ddg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public String getDdgJson() {
        return ddgJson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column fileresult_re.ddg_json
     *
     * @param ddgJson the value for fileresult_re.ddg_json
     *
     * @mbg.generated Thu Mar 24 21:27:19 CST 2022
     */
    public void setDdgJson(String ddgJson) {
        this.ddgJson = ddgJson == null ? null : ddgJson.trim();
    }
}