package com.hlc.codeanalyzesystem.entity.DDG;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DD_Node {
    private Integer key;
    private Integer line;
    private String label;
    private List<String> defs;
    private List<String> uses;
}
