package com.hlc.codeanalyzesystem.entity.CD;

import java.util.List;

import com.hlc.codeanalyzesystem.entity.AbstractGraphVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassDiagramVo extends AbstractGraphVo {
//    private String version="v2.0";
//    private String type;
//    private String label;
    private List<CD_Class> classes;
    private List<CD_Relation> relations;
}
