package com.hlc.codeanalyzesystem.entity.CFG;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CF_Edge {
    private Integer key;
    private Integer from;
    private Integer to;
    private String label;
}
