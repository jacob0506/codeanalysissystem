package com.hlc.codeanalyzesystem.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractGraphVo {
    protected String version="v2.0";
    protected String type;
    protected String label;
}
