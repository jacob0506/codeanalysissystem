package com.hlc.codeanalyzesystem.entity.CD;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CD_Method {
    private String methodName;
    private String modifier;
    private String return_type;
    private List<CD_Parameter> parameters;
}
