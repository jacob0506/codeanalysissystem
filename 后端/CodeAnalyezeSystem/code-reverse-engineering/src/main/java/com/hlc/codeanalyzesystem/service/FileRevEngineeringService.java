package com.hlc.codeanalyzesystem.service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hlc.codeanalyzesystem.config.Path;
import com.hlc.codeanalyzesystem.dao.FiledetailDao;
import com.hlc.codeanalyzesystem.dao.FilegraphDao;
import com.hlc.codeanalyzesystem.dao.FileresultReDao;
import com.hlc.codeanalyzesystem.dao.ProjectDao;
import com.hlc.codeanalyzesystem.entities.Filedetail;
import com.hlc.codeanalyzesystem.entities.Filegraph;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entity.CFG.ControlFlowGraphVo;
import com.hlc.codeanalyzesystem.entity.DDG.DataDependenceGraphVo;
import com.hlc.codeanalyzesystem.entity.FileDirNodeVo;
import com.hlc.codeanalyzesystem.entity.FileresultReExample;
import com.hlc.codeanalyzesystem.entity.FileresultReWithBLOBs;
import com.hlc.codeanalyzesystem.utils.FileUtil;
import com.hlc.codeanalyzesystem.utils.TransformUtil;
import ghaffarian.progex.graphs.cfg.ControlFlowGraph;
import org.apache.commons.io.FileUtils;
import ghaffarian.progex.graphs.pdg.DataDependenceGraph;
import ghaffarian.progex.java.JavaDDGBuilder;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import ghaffarian.progex.java.JavaCFGBuilder;

import java.io.*;
import java.util.*;

import static javax.swing.text.StyleConstants.Size;

@Service
public class FileRevEngineeringService {

    @Autowired
    private FileresultReDao fileresultReDao;

    @Autowired
    private FiledetailDao filedetailDao;

    @Autowired
    private FilegraphDao filegraphDao;

    @Autowired
    private ProjectDao projectDao;

    /**
     * 查找文件分析结果
     * @param pid 查找的项目id
     * @param filename 查找的文件的绝对路径
     * @return List<FileresultReWithBLOBs> 符合的分析结果列表
     */
    public List<FileresultReWithBLOBs> selectFileResultReByPidAndFilename(Integer pid, String filename){
        FileresultReExample fileresultReExample = new FileresultReExample();
        FileresultReExample.Criteria criteria = fileresultReExample.createCriteria();
        criteria.andBelongtoEqualTo(pid);
        criteria.andNameEqualTo(filename);
        return fileresultReDao.selectByExampleWithBLOBs(fileresultReExample);
    }

    public Filedetail selectFiledetailByPidAndFilename(Integer pid,String filename){
        /*FileresultExample fileresultExample = new FileresultExample();
        FileresultExample.Criteria criteria = fileresultExample.createCriteria();
        criteria.andBelongtoEqualTo(pid);
        criteria.andNameEqualTo(filename);*/
        return filedetailDao.selectByPidAndFilename(pid,filename);
    }

    public Filegraph selectFilegraphByFidAndType(Integer fid, Integer type){
        /*FileresultExample fileresultExample = new FileresultExample();
        FileresultExample.Criteria criteria = fileresultExample.createCriteria();
        criteria.andBelongtoEqualTo(pid);
        criteria.andNameEqualTo(filename);*/
        return filegraphDao.selectByFidAndType(fid,type);
    }

    /**
     * 控制流分析
     * @param pid 所分析的项目id
     * @param filename 所分析的项目中具体某个文件的绝对路径
     * @return HashMap<String,Object> 以hashmap数据结构表示的控制流图
     */
    public HashMap<String,Object> cfgAnalysis(Integer pid, String filename) throws Exception {
        List<FileresultReWithBLOBs> fileresultReList = selectFileResultReByPidAndFilename(pid,filename);
        HashMap<String,Object> cfg;
        //数据库中不存在对此文件控制流的分析
        if(fileresultReList == null || fileresultReList.size() == 0 || fileresultReList.get(0).getCfgJson()==null){
            //进行控制流分析
            System.out.println("控制流分析");
            ControlFlowGraph controlFlowGraph=JavaCFGBuilder.build(filename);
//            controlFlowGraph.getProperty("");
//            Iterator<CFNode> allVertices= controlFlowGraph.allVerticesIterator();
//            for(CFNode node=allVertices.next(); allVertices.hasNext();){
//                node.getLineOfCode();
//            }
//            Iterator<Edge<CFNode,CFEdge>> allEdges= controlFlowGraph.allEdgesIterator();
            cfg=controlFlowGraph.transToMap();
            cfg.put("version","v2.0");

            FileresultReWithBLOBs record=new FileresultReWithBLOBs();
            record.setBelongto(pid);
            record.setName(filename);
            record.setCfgJson(JSON.toJSONString(cfg));
            if(fileresultReList == null || fileresultReList.size() == 0){//不存在对此文件的任何分析
                fileresultReDao.insertSelective(record);
            }
            else{
                FileresultReExample example=new FileresultReExample();
                FileresultReExample.Criteria criteria=example.createCriteria();
                criteria.andBelongtoEqualTo(pid);
                criteria.andNameEqualTo(filename);
                fileresultReDao.updateByExampleSelective(record,example);
            }
        }
        else{
            System.out.println("从数据库提取");
            cfg = JSON.parseObject(fileresultReList.get(0).getCfgJson(),HashMap.class);
        }
        return cfg;
    }
    /**
     * 控制流分析
     * @param pid 所分析的项目id
     * @param filename 所分析的项目中具体某个文件的绝对路径
     * @return ControlFlowGraphVo 控制流图视图对象
     */
    public ControlFlowGraphVo cfgAnalysis2(Integer pid, String filename) throws Exception {
        /*List<FileresultReWithBLOBs> fileresultReList = selectFileResultReByPidAndFilename(pid,filename);*/
        Filedetail filedetail = filedetailDao.selectByPidAndFilename(pid,filename);
        Filegraph filegraph = filegraphDao.selectByFidAndType(filedetail.getId(),2);
        ControlFlowGraphVo cfg;
        //数据库中不存在对此文件控制流的分析
        if(filegraph==null){
            //进行控制流分析
            Project project = projectDao.selectByPrimaryKey(pid);
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            List<FileDirNodeVo> treeList = new ArrayList<>();
            for(FileDirNodeVo fileDirNodeVo : fileDirNodeVos){
                if(fileDirNodeVo.getNodePath().equals(filename)){
                    Integer[] temp = fileDirNodeVo.getResult();
                    temp[1]=1;
                    fileDirNodeVo.setResult(temp);
                }
                treeList.add(fileDirNodeVo);
            }
            project.setProjectdirjsonpath(JSON.toJSONString(treeList));
            projectDao.updateByPrimaryKeySelective(project);
            System.out.println("控制流分析");
            ControlFlowGraph controlFlowGraph=JavaCFGBuilder.build(filename);
            cfg= TransformUtil.transformCFGtoVo(controlFlowGraph);
            /*FileresultReWithBLOBs record=new FileresultReWithBLOBs();*/
            Filegraph temp = new Filegraph();
            temp.setFid(filedetail.getId());
            temp.setType(2);
            temp.setState(2);
            filegraphDao.insertSelective(temp);
            if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
            }

            String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + temp.getId() +".json";
            File file =new File(dir);
            if(!file.exists()){
                file.createNewFile();
            }
            try{
                FileUtils.write(file, JSON.toJSONString(cfg), "utf-8", false);
            }catch (Exception e){
                System.out.println("cfg error");
            }
            filegraphDao.updateByIdAndJsonPath(temp.getId(),dir);
            return cfg;
        }
        else{
            if(filegraph.getState()==2) {
                System.out.println("从数据库提取");
                String cfgGraphJson;
                File file = new File(filegraph.getGraphjsonpath());
                cfgGraphJson = FileUtils.readFileToString(file, "UTF-8");
                cfg = JSON.parseObject(cfgGraphJson,ControlFlowGraphVo.class);
                return cfg;
            }
            else {
                ControlFlowGraph controlFlowGraph=JavaCFGBuilder.build(filename);
                cfg= TransformUtil.transformCFGtoVo(controlFlowGraph);
                if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                    FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
                }

                String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + filegraph.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(cfg), "utf-8", false);
                }catch (Exception e){

                }
                filegraphDao.updateByIdAndJsonPath(filegraph.getId(),dir);
                return cfg;
            }
        }
    }
    /**
     * 数据依赖分析
     * @param pid 所分析的项目id
     * @param filename 所分析的项目中具体某个文件的绝对路径
     * @param withControlFlow 数据依赖是否同控制流一起输出
     * @return HashMap<String,Object> 以hashmap数据结构表示的数据依赖
     */
    public HashMap<String,Object> ddgAnalysis(Integer pid, String filename,boolean withControlFlow) throws Exception {
        List<FileresultReWithBLOBs> fileresultReList = selectFileResultReByPidAndFilename(pid,filename);
        HashMap<String,Object> ddg;
        boolean norecord = fileresultReList == null || fileresultReList.size() == 0;//数据库中不存在此文件的任何记录
        boolean notexist = !norecord&&withControlFlow&&fileresultReList.get(0).getDdgwithctrlJson()==null;//不存在此文件的ddgWithCtrl
        boolean notexist2=!norecord&&!withControlFlow&&fileresultReList.get(0).getDdgJson()==null;//不存在此文件的ddg
        //数据库中不存在对此文件数据依赖的分析
        if( norecord || notexist || notexist2 ){
            //进行控制流分析
            System.out.println("数据依赖分析");
            String[] filenames=new String[1];
            filenames[0]=filename;
            DataDependenceGraph[] dataDependenceGraph= JavaDDGBuilder.buildForAll(filenames);//分析
            ddg=dataDependenceGraph[0].transToMap(withControlFlow);
            ddg.put("version","v2.0");
            //System.out.println(ddg);
            //database访问
            FileresultReWithBLOBs record=new FileresultReWithBLOBs();
            record.setBelongto(pid);
            record.setName(filename);
            if(withControlFlow)
                record.setDdgwithctrlJson(JSON.toJSONString(ddg));
            else record.setDdgJson(JSON.toJSONString(ddg));
            if(norecord){//不存在对此文件的任何分析
                fileresultReDao.insertSelective(record);
            }
            else{
                FileresultReExample example=new FileresultReExample();
                FileresultReExample.Criteria criteria=example.createCriteria();
                criteria.andBelongtoEqualTo(pid);
                criteria.andNameEqualTo(filename);
                fileresultReDao.updateByExampleSelective(record,example);
            }
        }
        else{
            System.out.println("从数据库提取");
            if(withControlFlow)  ddg = JSON.parseObject(fileresultReList.get(0).getDdgwithctrlJson(),HashMap.class);
            else ddg = JSON.parseObject(fileresultReList.get(0).getDdgJson(),HashMap.class);

        }
        return ddg;
    }
    /**
     * 数据依赖分析
     * @param pid 所分析的项目id
     * @param filename 所分析的项目中具体某个文件的绝对路径
     * @param withControlFlow 数据依赖是否同控制流一起输出
     * @return DataDependenceGraphVo 数据依赖视图对象
     */
    public DataDependenceGraphVo ddgAnalysis2(Integer pid, String filename, boolean withControlFlow) throws Exception {
        Filedetail filedetail = selectFiledetailByPidAndFilename(pid,filename);
        new Filegraph();
        Filegraph filegraph;
        if(withControlFlow) {
            filegraph = selectFilegraphByFidAndType(filedetail.getId(),3);
        }
        else {
            filegraph = selectFilegraphByFidAndType(filedetail.getId(), 4);
        }
        DataDependenceGraphVo ddg;
        //数据库中不存在此文件的ddgWithCtrl或ddg
        //数据库中不存在对此文件数据依赖的分析
        if( filegraph == null ){
            //进行控制流分析
            Project project = projectDao.selectByPrimaryKey(pid);
            List<FileDirNodeVo> treeList = new ArrayList<>();
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            System.out.println("数据依赖分析");
            String[] filenames=new String[1];
            filenames[0]=filename;
            System.out.println(Arrays.toString(filenames));
            DataDependenceGraph[] dataDependenceGraph= JavaDDGBuilder.buildForAll(filenames);//分析
            //ddg=dataDependenceGraph[0].transToMap(withControlFlow);
            ddg=TransformUtil.transformDDGtoVo(dataDependenceGraph[0],withControlFlow);
            //ddg.put("version","v2.0");
            //System.out.println(ddg);
            //database访问
            /*FileresultReWithBLOBs record=new FileresultReWithBLOBs();*/
            Filegraph temp = new Filegraph();
            temp.setState(2);
            temp.setFid(filedetail.getId());
            /*record.setBelongto(pid);
            record.setName(filename);*/
            if(withControlFlow) {
                /*record.setDdgwithctrlJson(JSON.toJSONString(ddg));*/
                temp.setType(3);
                for(FileDirNodeVo fileDirNodeVo : fileDirNodeVos){
                    if(fileDirNodeVo.getNodePath().equals(filename)){
                        Integer[] t = fileDirNodeVo.getResult();
                        t[2]=1;
                        fileDirNodeVo.setResult(t);
                    }
                    treeList.add(fileDirNodeVo);
                }
                project.setProjectdirjsonpath(JSON.toJSONString(treeList));
                projectDao.updateByPrimaryKeySelective(project);
            }
            else {
               /* record.setDdgJson(JSON.toJSONString(ddg));*/
                temp.setType(4);
                for(FileDirNodeVo fileDirNodeVo : fileDirNodeVos){
                    if(fileDirNodeVo.getNodePath().equals(filename)){
                        Integer[] t = fileDirNodeVo.getResult();
                        t[3]=1;
                        fileDirNodeVo.setResult(t);
                    }
                    treeList.add(fileDirNodeVo);
                }
                project.setProjectdirjsonpath(JSON.toJSONString(treeList));
                projectDao.updateByPrimaryKeySelective(project);
            }
                /*fileresultReDao.insertSelective(record);*/
                filegraphDao.insertSelective(temp);
                if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
                }
                String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + temp.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(ddg), "utf-8", false);
                }catch (Exception e){
                    System.out.println("ddg error");
                }
                filegraphDao.updateByIdAndJsonPath(temp.getId(),dir);
                return ddg;
        }
        else if(filegraph.getState()==2){
            System.out.println("从数据库提取");
            String ddgGraphJson;
            File file = new File(filegraph.getGraphjsonpath());
            ddgGraphJson = FileUtils.readFileToString(file, "UTF-8");
            ddg = JSON.parseObject(ddgGraphJson,DataDependenceGraphVo.class);
            return ddg;
        }
        else {
            String[] filenames=new String[1];
            filenames[0]=filename;
            System.out.println(Arrays.toString(filenames));
            DataDependenceGraph[] dataDependenceGraph= JavaDDGBuilder.buildForAll(filenames);//分析
            //ddg=dataDependenceGraph[0].transToMap(withControlFlow);
            ddg=TransformUtil.transformDDGtoVo(dataDependenceGraph[0],withControlFlow);
            if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
            }
            String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + filegraph.getId() +".json";
            File file =new File(dir);
            if(!file.exists()){
                file.createNewFile();
            }
            try{
                FileUtils.write(file, JSON.toJSONString(ddg), "utf-8", false);
            }catch (Exception e){
                System.out.println("ddg error");
            }
            filegraphDao.updateByIdAndJsonPath(filegraph.getId(),dir);
            return ddg;
        }
    }
}
