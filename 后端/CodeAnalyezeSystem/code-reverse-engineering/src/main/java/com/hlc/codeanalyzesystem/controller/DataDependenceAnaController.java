package com.hlc.codeanalyzesystem.controller;

import com.alibaba.fastjson.JSON;
import com.hlc.codeanalyzesystem.entity.DDG.DataDependenceGraphVo;
import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.service.FileRevEngineeringService;
import com.hlc.codeanalyzesystem.service.ProjectService;
import com.hlc.codeanalyzesystem.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;

@RestController
@RequestMapping("/dataDependence")
@CrossOrigin
@Slf4j
public class DataDependenceAnaController {

    @Autowired
    private FileRevEngineeringService fileRevEngineeringService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private RecordService recordService;

    //响应分析数据依赖图
    //filename:绝对路径
    @RequestMapping("/analysis/{pid}")
    public ResultJSON ddgAnalysis(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId, @RequestParam("withCtrl") boolean withCtrl){
        try{
            String projectName=projectService.queryProjectNameByPid(pid);
            DataDependenceGraphVo ddg = fileRevEngineeringService.ddgAnalysis2(pid,fileName,withCtrl);
            /*if(ddg.isEmpty()){
                return new ResultJSON(0,"分析结果为空",ddg);
            }*/
            String operation= withCtrl?"DDGWithCtrlAnalyze":"DDGAnalyze";
            /*recordService.insertRecord(userId,pid,projectName+":"+fileName,operation,"/reengineering");*/
            return new ResultJSON(1,"数据依赖分析成功",ddg);
        }
        catch (Exception e){
            e.printStackTrace();
            log.info("ddgAnalysis exception  " + fileName);
            return new ResultJSON(-1,"数据依赖分析失败",null);
        }
    }

    //导出数据依赖
    @RequestMapping("/export/{pid}")
    public int exportDDG(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId, @RequestParam("withCtrl") boolean withCtrl,HttpServletResponse response) {
        try {
            if (fileName != null) {
                String exports = JSON.toJSONString(fileRevEngineeringService.ddgAnalysis2(pid, fileName, withCtrl));
                //System.out.println(exports);
                // 以流的形式下载文件
                InputStream in = new BufferedInputStream(new ByteArrayInputStream(exports.getBytes()));
                byte[] buffer = new byte[in.available()];
                in.read(buffer);
                in.close();
                String downloadFileName = System.currentTimeMillis() + ".json";
                response.reset();
                response.setContentType("application/octet-stream");// 设置强制下载不打开
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Content-Disposition", "attachment;fileName=" + downloadFileName);// 设置文件名
                try {
                    OutputStream os = new BufferedOutputStream(response.getOutputStream());
                    os.write(buffer);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    log.info("ddg export exception");
                    return -1;
                }
            }
            return 0;
        } catch (Exception e) {
            log.info("ddg export exception");
            return -1;
        }
    }
}
