package com.hlc.codeanalyzesystem.entity.CD;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CD_Relation {
    private int from;
    private int to;
    private String relationship;
}
