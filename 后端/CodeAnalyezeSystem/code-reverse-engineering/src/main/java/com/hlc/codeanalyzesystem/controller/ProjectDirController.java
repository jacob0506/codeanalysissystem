package com.hlc.codeanalyzesystem.controller;

import com.alibaba.fastjson.JSON;
import com.hlc.codeanalyzesystem.entity.FileDirNodeVo;
import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.service.ProjectRevEngineeringService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/projectDir")
@CrossOrigin
@Slf4j
public class ProjectDirController {

    @Autowired
    private ProjectRevEngineeringService projectRevEngineeringService;

    //响应输出文件目录
    @RequestMapping("/{pid}")
    public ResultJSON analyzeProjectDir(@PathVariable("pid") Integer pid){
        try{
            List<FileDirNodeVo> list=projectRevEngineeringService.analyzeProjectDir(pid);
            if(list.isEmpty()){
                return new ResultJSON(0,"目录为空",list);
            }
            return new ResultJSON(1,"获取目录成功",list);
        }
        catch (Exception e){
            e.printStackTrace();
            log.info("outputProjectDir" + pid);
            return new ResultJSON(-1,"获取目录失败",null);
        }
    }
}
