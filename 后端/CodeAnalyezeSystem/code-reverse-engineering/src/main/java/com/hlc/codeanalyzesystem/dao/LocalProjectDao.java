package com.hlc.codeanalyzesystem.dao;

import com.hlc.codeanalyzesystem.entities.Localproject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LocalProjectDao {

    Localproject selectByPid(Integer pid);

    int insertSelective(Localproject localproject);

}
