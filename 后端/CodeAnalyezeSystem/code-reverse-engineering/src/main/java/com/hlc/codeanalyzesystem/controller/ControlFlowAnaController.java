package com.hlc.codeanalyzesystem.controller;

import com.alibaba.fastjson.JSON;
import com.hlc.codeanalyzesystem.entity.CFG.ControlFlowGraphVo;
import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.service.FileRevEngineeringService;
import com.hlc.codeanalyzesystem.service.ProjectService;
import com.hlc.codeanalyzesystem.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;


@RestController
@RequestMapping("/controlFlow")
@CrossOrigin
@Slf4j
public class ControlFlowAnaController {

    @Autowired
    private FileRevEngineeringService fileRevEngineeringService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private RecordService recordService;


    //响应分析控制流图
    //filename:绝对路径
    @RequestMapping("/analysis/{pid}")
    public ResultJSON cfgAnalysis(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId){
        try{
            String projectName=projectService.queryProjectNameByPid(pid);
            String dir = "C:\\Users\\86181\\代码分析系统\\后端\\Project\\15\\main\\java\\com\\messagesystem\\web\\InviteController.java";
            ControlFlowGraphVo cfg = fileRevEngineeringService.cfgAnalysis2(pid,fileName);
            /*recordService.insertRecord(userId,pid,projectName+":"+fileName,"CFGAnalyze","/reengineering");*/
            return new ResultJSON(1,"控制流分析成功",cfg);
        }
        catch (Exception e){
            e.printStackTrace();
            log.info("cfgAnalysis exception  " + fileName);
            return new ResultJSON(-1,"控制流分析失败",null);
        }


    }

    //导出控制流图
    @RequestMapping("/export/{pid}")
    public int exportCFG(@PathVariable("pid") Integer pid, @RequestParam("fileName") String fileName, @RequestParam("userId") Integer userId, HttpServletResponse response){
        try {
            if (fileName != null) {
                String exports = JSON.toJSONString(fileRevEngineeringService.cfgAnalysis2(pid, fileName));
                //System.out.println(exports);
                // 以流的形式下载文件
                InputStream in = new BufferedInputStream(new ByteArrayInputStream(exports.getBytes()));
                byte[] buffer = new byte[in.available()];
                in.read(buffer);
                in.close();
                String downloadFileName = System.currentTimeMillis()+".json";
                response.reset();//清除response中的缓存信息
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Content-Disposition", "attachment;fileName=" + downloadFileName);// 设置文件名
                try {
                    OutputStream os = new BufferedOutputStream(response.getOutputStream());
                    os.write(buffer);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    log.info("cfg export exception");
                    return -1;
                }
            }
            return 0;
        } catch (Exception e) {
            log.info("cfg export exception");
            return -1;
        }
    }

}
