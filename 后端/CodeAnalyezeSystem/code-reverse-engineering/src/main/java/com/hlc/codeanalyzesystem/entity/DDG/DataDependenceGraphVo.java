package com.hlc.codeanalyzesystem.entity.DDG;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hlc.codeanalyzesystem.entity.AbstractGraphVo;
import com.hlc.codeanalyzesystem.entity.CFG.CF_Edge;
import com.hlc.codeanalyzesystem.entity.CFG.CF_Node;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataDependenceGraphVo extends AbstractGraphVo {
//    private String version="v2.0";
//    private String type;
//    private String label;
    @JsonProperty("file")
    private String fileName;
    private List<DD_Node> nodes;
    private List<DD_Edge> edges;
    private boolean directed;
    private boolean multigraph;
}
