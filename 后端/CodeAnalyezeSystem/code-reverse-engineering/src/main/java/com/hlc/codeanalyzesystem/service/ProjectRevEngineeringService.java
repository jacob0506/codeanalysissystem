package com.hlc.codeanalyzesystem.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hlc.codeanalyzesystem.config.Path;
import com.hlc.codeanalyzesystem.dao.LocalProjectDao;
import com.hlc.codeanalyzesystem.dao.ProjectDao;
import com.hlc.codeanalyzesystem.dao.ProjectGraphDao;
import com.hlc.codeanalyzesystem.dao.ProjectresultReDao;
import com.hlc.codeanalyzesystem.entities.Localproject;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entities.Projectgraph;
import com.hlc.codeanalyzesystem.entity.CD.ClassDiagramVo;
import com.hlc.codeanalyzesystem.entity.FileDirNodeVo;
import com.hlc.codeanalyzesystem.entity.ProjectresultRe;
import com.hlc.codeanalyzesystem.entity.ProjectresultReExample;
import com.hlc.codeanalyzesystem.utils.FileUtil;
import com.hlc.codeanalyzesystem.utils.TransformUtil;
import com.uml.parser.main.Counselor;
import com.uml.parser.main.GenerateUML;
import com.uml.parser.main.ParseJava;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;


@Service
public class ProjectRevEngineeringService {

    @Autowired
    private ProjectresultReDao projectresultReDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ProjectGraphDao projectGraphDao;

    @Autowired
    private LocalProjectDao localProjectDao;

    public static final String  resourcePath = Path.Project_dir;//项目存储路径
    private int dirId;//目录结构起始id
    private List<FileDirNodeVo> projectDir;//目录结构



    /**
     * 类图分析
     * @param pid 所分析的项目id
     * @param projectName 所分析的项目名称
     * @return ClassDiagramVo 类图视图对象
     */
    public ClassDiagramVo cdAnalysis2(Integer pid, String projectName) throws Exception {
        String projectPath = resourcePath + pid + File.separator ;
        File folder = new File(projectPath);
        if (folder == null || !folder.isDirectory()) {
            System.out.println("Folder path provided is not valid, please check -> " + projectPath);
            throw new Exception("文件路径错误");
        }
        /*ProjectresultRe projectresultRe=projectresultReDao.selectByPrimaryKey(pid);*/
        Projectgraph projectgraph = projectGraphDao.selectByPidAndType(pid,3);
        if(projectgraph!=null&&projectgraph.getState()==2){
            System.out.println("从数据库提取");
            String cdGraphJson;
            File file = new File(projectgraph.getGraphjsonpath());
            cdGraphJson = FileUtils.readFileToString(file, "UTF-8");
            return JSON.parseObject(cdGraphJson, ClassDiagramVo.class);
        }
        else{
            List<File> files =new ArrayList<>();
            FileUtil.getAllFilesFromFolder(folder,files);
            if(files.isEmpty()){
                throw new Exception("上传项目中不存在java文件");
            }
            ParseJava obj = new ParseJava();
            boolean parseSucceed = obj.parseFiles(files);
            if(!parseSucceed) {
                throw new Exception("项目文件解析失败");
            }
            ClassDiagramVo classDiagramVo=TransformUtil.transformCDtoVo(Counselor.getInstance());
            classDiagramVo.setLabel("CD of " +  projectName);
            //访问数据库
            Projectgraph temp = new Projectgraph();
            temp.setPid(pid);
            temp.setState(2);
            temp.setType(3);
            /*ProjectresultRe record=new ProjectresultRe();
            record.setId(pid);
            record.setCdJson(JSON.toJSONString(classDiagramVo));*/
            if(projectgraph==null) {
                Project project = projectDao.selectByPrimaryKey(pid);
                List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
                Integer[] t = fileDirNodeVos.get(0).getResult();
                t[2]=1;
                fileDirNodeVos.get(0).setResult(t);
                project.setProjectdirjsonpath(JSON.toJSONString(fileDirNodeVos));
                projectDao.updateByPrimaryKeySelective(project);
                projectGraphDao.insertSelective(temp);
                if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "projectgraph" + File.separator)){
                    FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "projectgraph" + File.separator);
                }
                String dir = Path.Project_dir + pid + File.separator + "projectgraph" + File.separator + temp.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(classDiagramVo), "utf-8", false);
                }catch (Exception e){
                    System.out.println("cd error");
                }
                projectGraphDao.updateByIdAndJsonPath(temp.getId(),dir);
            }
            else if(projectgraph!=null&&projectgraph.getState()==1){
                String dir = Path.Project_dir + pid + File.separator + "projectgraph" + File.separator + projectgraph.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(classDiagramVo), "utf-8", false);
                }catch (Exception e){
                    System.out.println("cd error");
                }
                projectGraphDao.updateByIdAndJsonPath(projectgraph.getId(),dir);
            }
            return classDiagramVo;
        }
    }
    /**
     * 类图分析
     * @param pid 所分析的项目id
     * @param projectName 所分析的项目名称
     * @return HashMap<String,Object> 以hashmap数据结构表示的类图
     */
    public HashMap<String,Object> cdAnalysis(Integer pid,String projectName) throws Exception {
        String projectPath = resourcePath + pid + File.separator;
        File folder = new File(projectPath);
        if (folder == null || !folder.isDirectory()) {
            System.out.println("Folder path provided is not valid, please check -> " + projectPath);
            throw new Exception("文件路径错误");
        }
        ProjectresultRe projectresultRe=projectresultReDao.selectByPrimaryKey(pid);
        if(projectresultRe!=null&&projectresultRe.getCdJson()!=null){
            System.out.println("从数据库提取");
            return JSON.parseObject(projectresultRe.getCdJson(),HashMap.class);
        }
        else {
            List<File> files = new ArrayList<>();
            FileUtil.getAllFilesFromFolder(folder, files);
            if (files.isEmpty()) {
                throw new Exception("上传项目中不存在java文件");
            }
            ParseJava obj = new ParseJava();
            //obj.parseFiles(files);
            boolean parseSucceed = obj.parseFiles(files);
            if (!parseSucceed) {
                throw new Exception("项目文件解析失败");
            }
            GenerateUML generateUML = new GenerateUML();
            HashMap<String, Object> cdMap = generateUML.createGrammar();
            cdMap.put("label", "CD of " + projectName);
            cdMap.put("type", "Class Diagram (CD)");
            cdMap.put("version", "v2.0");
            //访问数据库
            ProjectresultRe record = new ProjectresultRe();
            record.setId(pid);
            record.setCdJson(JSON.toJSONString(cdMap));
            if (projectresultRe == null) projectresultReDao.insertSelective(record);
            else {
                ProjectresultReExample example = new ProjectresultReExample();
                example.createCriteria().andIdEqualTo(pid);
                projectresultReDao.updateByExampleSelective(record, example);
            }
            return cdMap;
        }
    }
    /**
     * 分析目录结构
     * @param id 所分析项目的id
     * @return List<FileDirNodeVo> 该项目的文件结点集合（树结构）
     */
    public List<FileDirNodeVo> analyzeProjectDir(Integer id) throws Exception{
        /*ProjectresultRe projectresultRe=projectresultReDao.selectByPrimaryKey(id);*/
        Project project = projectDao.selectByPrimaryKey(id);
        //数据库内存在记录
        if(project!=null&&project.getProjectdirjsonpath()!=null&&project.getProjectdirjsonpath().length()>1){
            List<FileDirNodeVo> projectDir=JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            return TransformUtil.buildDirTree(projectDir,0);
        }
        /*if(projectresultRe!=null&&projectresultRe.getDirstructure()!=null){
            List<FileDirNodeVo> projectDirVo=JSON.parseObject(projectresultRe.getDirstructure(),new TypeReference<List<FileDirNodeVo>>(){});
            return TransformUtil.buildDirTree(projectDirVo,0);
        }*/
        //数据库不存在记录
        else {
            Localproject localproject = localProjectDao.selectByPid(id);
            if(localproject!=null) {
                System.out.println("不存在记录");
                dirId = 1;
                projectDir = new ArrayList<>();
                String projectPath = resourcePath + id + File.separator;
                //String projectPath = "/Users/katerina/Desktop/test1";
                dfsFile(projectPath, 0);
                Project record = new Project();
                record.setId(id);
                record.setProjectdirjsonpath(JSON.toJSONString(projectDir));
                if (project == null) projectDao.insertSelective(record);
                else {
                    projectDao.updateByPrimaryKeySelective(record);
                }
                return TransformUtil.buildDirTree(projectDir, 0);
                //return projectDirVo;
            }
            else {
                return null;
            }
        }
    }


    /**
     * 深度遍历项目文件
     * @param filePath 所遍历的项目文件的路径
     * @param pid 首个节点迭代的父节点id,默认为0
     */
    public void dfsFile(String filePath,int pid){
        File f = new File(filePath);
        for(File file : f.listFiles())
        {
            if(file.isDirectory())
            {
                Integer[] result = {0,0,0};
                projectDir.add(new FileDirNodeVo(dirId,pid,file.getName(),file.getAbsolutePath(),result));
                int tempId = dirId;
                dirId++;
                dfsFile(file.getAbsolutePath(),tempId);
            }
            else if(file.getName().endsWith(".java"))
            {
                Integer[] result = {0,0,0,0};
                projectDir.add(new FileDirNodeVo(dirId,pid,file.getName(),file.getAbsolutePath(),result));
                dirId++;
            }
            else {
                Integer[] result = {0,0,0};
                projectDir.add(new FileDirNodeVo(dirId,pid,file.getName(),file.getAbsolutePath(),result));
                dirId++;
            }
        }
    }

}
