package com.hlc.codeanalyzesystem.utils;

import com.hlc.codeanalyzesystem.entity.CD.*;
import com.hlc.codeanalyzesystem.entity.CFG.CF_Edge;
import com.hlc.codeanalyzesystem.entity.CFG.CF_Node;
import com.hlc.codeanalyzesystem.entity.CFG.ControlFlowGraphVo;
import com.hlc.codeanalyzesystem.entity.DDG.DD_Edge;
import com.hlc.codeanalyzesystem.entity.DDG.DD_Node;
import com.hlc.codeanalyzesystem.entity.DDG.DataDependenceGraphVo;
import com.hlc.codeanalyzesystem.entity.FileDirNodeVo;
import com.uml.parser.enums.Modifiers;
import com.uml.parser.enums.RelationType;
import com.uml.parser.main.Counselor;
import com.uml.parser.main.UMLHelper;
import com.uml.parser.model.Relationship;
import com.uml.parser.model.UMLClass;
import com.uml.parser.model.UMLMethod;
import com.uml.parser.model.UMLVariable;
import ghaffarian.graphs.Edge;
import ghaffarian.nanologger.Logger;
import ghaffarian.progex.graphs.cfg.CFEdge;
import ghaffarian.progex.graphs.cfg.CFNode;
import ghaffarian.progex.graphs.cfg.ControlFlowGraph;
import ghaffarian.progex.graphs.pdg.DDEdge;
import ghaffarian.progex.graphs.pdg.DataDependenceGraph;
import ghaffarian.progex.graphs.pdg.PDNode;
import ghaffarian.progex.utils.StringUtils;
import japa.parser.ast.body.Parameter;

import java.util.*;
/***
 * 转换工具类
 */
public class TransformUtil {

    public static List<FileDirNodeVo> buildDirTree(List<FileDirNodeVo> list, int pid){
        List<FileDirNodeVo> treeList = new ArrayList<>();
        for (FileDirNodeVo treeNodeVo : list) {
            if (treeNodeVo.getPid() == pid) {
                treeNodeVo.setChildren(buildDirTree(list, treeNodeVo.getId()));
                treeList.add(treeNodeVo);
            }
        }
        return treeList;
    }
    public static CD_Method transformMethod(UMLMethod method){
        CD_Method cd_method=new CD_Method();

        if(method.getParameters() != null){
            cd_method.setModifier(Modifiers.valueOf(method.getModifier()));
            cd_method.setMethodName(method.getName());
            List<CD_Parameter> paramList=new ArrayList<>();

            List<Parameter> oldParamList=method.getParameters();
            for(int i=0; i < oldParamList.size(); i++){
                CD_Parameter cd_parameter=new CD_Parameter(oldParamList.get(i).getId().getName(),oldParamList.get(i).getType().toString());
                paramList.add(cd_parameter);
            }
            cd_method.setParameters(paramList);
        }else {//方法的参数为空
            List<CD_Parameter> paramList=new ArrayList<>();
            cd_method.setModifier(Modifiers.valueOf(method.getModifier()));
            cd_method.setMethodName(method.getName());
            cd_method.setParameters(paramList);//方法参数
        }

        if(method.getType() != null)
            cd_method.setReturn_type(method.getType().toString());
        return cd_method;
    }
    public static CD_Property transformVariable(UMLVariable variable){
        return new CD_Property(variable.getName(), Modifiers.valueOf(variable.getModifier()),variable.getType().toString());
    }

    public static ClassDiagramVo transformCDtoVo(Counselor counselor){
        ClassDiagramVo classDiagramVo=new ClassDiagramVo();
        classDiagramVo.setType("Class Diagram (CD)");
        Integer key=0;//为每个class设置键值
        HashMap<String,Integer> nameToKeyMap =new HashMap<>();
        List<CD_Class> cd_classArray=new ArrayList<>();
        //遍历每个class
        for(UMLClass umlClass : counselor.getUMLClasses()){
            CD_Class cd_class=new CD_Class();
            if(umlClass.isInterface()){
                cd_class.setInterfaceOrNot(true);
            }else {
                cd_class.setInterfaceOrNot(false);
            }
            key++;
            nameToKeyMap.put(umlClass.getName(),key);
            cd_class.setKey(key);
            cd_class.setClassName(umlClass.getName());
            boolean hasSetter = false;
            boolean hasGetter = false;
            String setVariable = "";
            String getVariable = "";
            UMLMethod setterMethod = null;
            UMLMethod getterMethod = null;
            List<UMLMethod> methods = umlClass.getUMLMethods();
            //遍历类中方法
            List<CD_Method> methodsArray=new ArrayList<>();
            for(UMLMethod method : methods){
                if(!(method.getModifier() == Modifiers.PUBLIC.modifier || method.getModifier() == Modifiers.PUBLIC_STATIC.modifier || method.getModifier() == Modifiers.PUBLIC_ABSTRACT.modifier)){
                    continue;
                }
                if(method.isConstructor()){//是构造函数
                    methodsArray.add(transformMethod(method));
                }else if(method.getName().contains("set") && method.getName().split("set").length > 1){//是set方法
                    hasSetter = true;
                    setVariable = method.getName().split("set")[1];
                    setterMethod = method;
                }else if(method.getName().contains("get") && method.getName().split("get").length > 1){//是get方法
                    hasGetter = true;
                    getVariable = method.getName().split("get")[1];
                    getterMethod = method;
                }else if((method.getModifier() == Modifiers.PUBLIC.modifier || method.getModifier() == Modifiers.PUBLIC_STATIC.modifier || method.getModifier() == Modifiers.PUBLIC_ABSTRACT.modifier)){//是公有方法
                    methodsArray.add(transformMethod(method));
                }
                if(hasGetter && hasSetter && setVariable.equalsIgnoreCase(getVariable) && setterMethod != null){
                    if(umlClass.hasVariable(getVariable)){
                        counselor.updateVariableToPublic(umlClass, getVariable);
                        counselor.removeSetterGetterMethod(umlClass, getterMethod, setterMethod);
                    }else {
                        System.out.println(getterMethod.getName());
                        methodsArray.add(transformMethod(getterMethod));
                        methodsArray.add(transformMethod(setterMethod));
                    }
                    hasGetter=false;
                    hasSetter=false;
                }
            }
            cd_class.setMethods(methodsArray);
            //遍历类中属性
            List<CD_Property> variablesArray=new ArrayList<>();
            List<UMLVariable> variables = umlClass.getUMLVariables();
            for(UMLVariable variable : variables){
//				if(variable.getModifier() != Modifiers.PROTECTED.modifier && variable.getModifier() != Modifiers.PACKAGE.modifier &&
//						!variable.isUMLClassType()){
                if(variable.getModifier() != Modifiers.PROTECTED.modifier && variable.getModifier() != Modifiers.PACKAGE.modifier){
                    variablesArray.add(transformVariable(variable));
                }
            }
            cd_class.setProperties(variablesArray);
            cd_classArray.add(cd_class);

        }
        classDiagramVo.setClasses(cd_classArray);

        //遍历类间关系
        List<CD_Relation> cd_linkArray=new ArrayList<>();
        for(Relationship relationship : counselor.getRelationships()){

            CD_Relation cd_relation=new CD_Relation(-1,-1,"");
            if(relationship.getType() == RelationType.DEPENDENCY && UMLHelper.isInterfaceDependency(relationship)){
                cd_relation.setRelationship(relationship.getType().toString().toLowerCase());
                cd_relation.setFrom(nameToKeyMap.get(relationship.getChild().getName()));
                cd_relation.setTo(nameToKeyMap.get(relationship.getParent().getName()));
            }else if(relationship.getType() != RelationType.DEPENDENCY){
                cd_relation.setRelationship(relationship.getType().toString().toLowerCase());
                cd_relation.setFrom(nameToKeyMap.get(relationship.getChild().getName()));
                cd_relation.setTo(nameToKeyMap.get(relationship.getParent().getName()));

            }
            if(cd_relation.getFrom()!=-1)cd_linkArray.add(cd_relation);
        }
        classDiagramVo.setRelations(cd_linkArray);
        counselor.refresh();//以防每次运行累计上次的数据
        return classDiagramVo;
    }
    public static ControlFlowGraphVo transformCFGtoVo(ControlFlowGraph controlFlowGraph){

        ControlFlowGraphVo controlFlowGraphVo=new ControlFlowGraphVo();
        controlFlowGraphVo.setType("Control Flow Graph (CFG)");
        controlFlowGraphVo.setLabel("CFG of "+controlFlowGraph.fileName);
        controlFlowGraphVo.setDirected(true);
        controlFlowGraphVo.setMultigraph(true);
        controlFlowGraphVo.setPkgName(controlFlowGraph.getPackage());
        controlFlowGraphVo.setFileName(controlFlowGraph.fileName);

        List<CF_Node> listNodes = new ArrayList();
        Map<CFNode, Integer> nodeIDs = new LinkedHashMap<>();
        int nodeCounter = 0;

        for(Iterator<CFNode> verticesIterator= controlFlowGraph.allVerticesIterator();verticesIterator.hasNext();){
            CFNode cfNode=verticesIterator.next();
            CF_Node cf_node=new CF_Node(nodeCounter,cfNode.getLineOfCode(),StringUtils.escape(cfNode.getCode()));
            listNodes.add(cf_node);
            nodeIDs.put(cfNode, nodeCounter);
            nodeCounter++;
        }
        controlFlowGraphVo.setNodes(listNodes);

        List<CF_Edge> listEdges = new ArrayList();
        int edgeCounter = 0;
        for(Iterator<Edge<CFNode,CFEdge>> edgeIterator= controlFlowGraph.allEdgesIterator();edgeIterator.hasNext();){
            Edge<CFNode,CFEdge> edge=edgeIterator.next();
            CF_Edge cf_edge=new CF_Edge(edgeCounter,nodeIDs.get(edge.source),nodeIDs.get(edge.target),edge.label.type.toString());
            listEdges.add(cf_edge);
            edgeCounter++;
        }
        controlFlowGraphVo.setEdges(listEdges);
        Logger.info("CFG converted to hashmap");
        return controlFlowGraphVo;
    }
    public static DataDependenceGraphVo transformDDGtoVo(DataDependenceGraph dataDependenceGraph,boolean withControlFlow){
        DataDependenceGraphVo dataDependenceGraphVo=new DataDependenceGraphVo();
        dataDependenceGraphVo.setType("Data Dependence Graph (DDG)");
        dataDependenceGraphVo.setLabel("DDG of "+ dataDependenceGraph.fileName);
        dataDependenceGraphVo.setDirected(true);
        dataDependenceGraphVo.setMultigraph(true);
        dataDependenceGraphVo.setFileName(dataDependenceGraph.fileName);
        if(withControlFlow) dataDependenceGraphVo.setType("Data Dependence Graph (DDG) with Control Flow");
        else dataDependenceGraphVo.setType("Data Dependence Graph (DDG)");
        List<DD_Node> listNodes = new ArrayList();
        Map<CFNode, Integer> ctrlNodes = new LinkedHashMap<>();
        Map<PDNode, Integer> dataNodes = new LinkedHashMap<>();
        Iterator<CFNode> cfNodes = dataDependenceGraph.getCFG().allVerticesIterator();
        int nodeCounter = 0;
        //节点
        while (cfNodes.hasNext()) {
            DD_Node dd_node=new DD_Node();
            CFNode node = cfNodes.next();
            PDNode pdNode = (PDNode) node.getProperty("pdnode");
            dd_node.setKey(nodeCounter);
            dd_node.setLine(node.getLineOfCode());
            //如果为数据依赖的节点
            if (pdNode != null) {
                dd_node.setLabel(StringUtils.escape(node.getCode()));
                dd_node.setDefs(Arrays.asList(pdNode.getAllDEFs()));
                dd_node.setUses(Arrays.asList(pdNode.getAllUSEs()));
                dataNodes.put(pdNode, nodeCounter);
            } else
                dd_node.setLabel(StringUtils.escape(node.getCode()));

            //如果此节点不为数据依赖结点且不输出控制流，则输出该节点
            if(pdNode!=null||withControlFlow) listNodes.add(dd_node);
            ctrlNodes.put(node, nodeCounter);
            ++nodeCounter;
            if (nodeCounter == dataDependenceGraph.getCFG().vertexCount()){
                dataDependenceGraphVo.setNodes(listNodes);
            }
            else {
                //json.println("    },");
            }
        }
        List<DD_Edge> listEdges = new ArrayList();
        int edgeCounter = 0;
        Iterator<Edge<CFNode, CFEdge>> cfEdges = dataDependenceGraph.getCFG().allEdgesIterator();
        //边 控制流
        while (cfEdges.hasNext()) {
            Edge<CFNode, CFEdge> ctrlEdge = cfEdges.next();
            if(withControlFlow){
                DD_Edge dd_edge=new DD_Edge(edgeCounter,ctrlNodes.get(ctrlEdge.source),ctrlNodes.get(ctrlEdge.target),ctrlEdge.label.type.toString(),"Control");
                listEdges.add(dd_edge);
            }
            ++edgeCounter;
        }
        //边 数据流


        for(Iterator<Edge<PDNode,DDEdge>> edgeIterator= dataDependenceGraph.allEdgesIterator();edgeIterator.hasNext();){
            Edge<PDNode, DDEdge> dataEdge=edgeIterator.next();
            DD_Edge dd_edge=new DD_Edge(edgeCounter,dataNodes.get(dataEdge.source),dataNodes.get(dataEdge.target),dataEdge.label.var,dataEdge.label.type.toString());
            listEdges.add(dd_edge);
            ++edgeCounter;
            if (edgeCounter == dataDependenceGraph.getCFG().edgeCount() + dataDependenceGraph.edgeCount()){
                dataDependenceGraphVo.setEdges(listEdges);
            }
            else{
//				json.println("    },");
            }
        }
        //不存在数据依赖
        if(  dataDependenceGraph.edgeCount()==0){
            dataDependenceGraphVo.setEdges(listEdges);
        }
        Logger.info("DDG converted");
        return dataDependenceGraphVo;
    }
}
