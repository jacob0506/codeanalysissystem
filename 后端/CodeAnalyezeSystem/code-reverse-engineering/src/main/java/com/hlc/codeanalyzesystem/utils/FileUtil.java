package com.hlc.codeanalyzesystem.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
/***
 * 文件处理工具类
 */
public class FileUtil {
    /**
     * Read all java files from the folder
     * @param folder
     * @param files store the java file added
     * @return
     */
    public static void getAllFilesFromFolder(File folder,List<File> files) {
        File[] filesInFolder = folder.listFiles();
        for (int i = 0; filesInFolder != null && i < filesInFolder.length; i++) {
            File file = filesInFolder[i];
            if (file.isDirectory()) {
                getAllFilesFromFolder(file, files);
            } else if (isValidFile(file)) {
                files.add(file);
            }
        }
    }
    /**
     * Returns list of java files from the folder
     * @param folder
     * @return
     */
    public static List<File> getFileListFromFolder(File folder) {
        List<File> files = new ArrayList<>();
        File[] filesInFolder = folder.listFiles();
        for (int i = 0; filesInFolder != null && i < filesInFolder.length; i++) {
            File file = filesInFolder[i];
            if (isValidFile(file)) {
                files.add(file);
            }
        }
        return files;
    }
    /**
     * Returns if the file is valid Java file or not
     * @param file
     * @return
     */
    public static boolean isValidFile(File file) {
        if (file.isFile()) {
            if (file.getName().endsWith("java")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns if the file is a file
     * @param filepath
     * @return
     */
    public static boolean isFile(String filepath) {
        File f = new File(filepath);
        return f.exists() && f.isFile();
    }
    /**
     * Returns if the file is a dir
     * @param dirPath
     * @return
     */
    public static boolean isDir(String dirPath) {
        File f = new File(dirPath);
        return f.exists() && f.isDirectory();
    }

    /**
     * 创建多级目录
     * @param path
     */
    public static void makeDirs(String path) {
        File file = new File(path);
        // 如果文件夹不存在则创建
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }else {
            System.out.println("创建目录失败："+path);
        }
    }
}
