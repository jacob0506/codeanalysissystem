package com.hlc.codeanalyzesystem.entity.CD;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CD_Class {
    private int key;
    private String className;//类名
    private boolean interfaceOrNot;//是否为接口
    private List<CD_Property> properties=new ArrayList();//成员变量
    private List<CD_Method>methods=new ArrayList();//成员方法


}
