package com.hlc.codeanalyzesystem.entity.DDG;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DD_Edge {
    private Integer key;
    private Integer from;
    private Integer to;
    private String label;
    private String type;
}
