package com.hlc.codeanalyzesystem.service;


import com.hlc.codeanalyzesystem.dao.ProjectDao;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entities.ProjectExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class ProjectService {

    //public static final String PROJECT_DIR = "/Users/katerina/Desktop/codesystem/CodeSystem/resources/";

    @Autowired
    ProjectDao projectDao;

    public List<Project> queryProjectByUserId(Integer userId){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        return projectList;
    }

    public String queryProjectNameByPid(Integer proId){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andIdEqualTo(proId);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        if(projectList.isEmpty()) return "";
        return projectList.get(0).getProjectname();
    }

    public List<Project> queryProjectByUserIdAndPageNo(Integer userId,Integer pageNo){
        ProjectExample projectExample = new ProjectExample();
        ProjectExample.Criteria criteria = projectExample.createCriteria();
        criteria.andUseridEqualTo(userId);
        projectExample.setLeftLimit((pageNo - 1) * 8);
        projectExample.setLimitSize(8);
        List<Project> projectList = projectDao.selectByExample(projectExample);
        return projectList;
    }





}
