package com.hlc.codeanalyzesystem.controller;

import com.alibaba.fastjson.JSON;
import com.hlc.codeanalyzesystem.entities.Projectgraph;
import com.hlc.codeanalyzesystem.entity.CD.ClassDiagramVo;
import com.hlc.codeanalyzesystem.entity.ResultJSON;
import com.hlc.codeanalyzesystem.service.ProjectRevEngineeringService;
import com.hlc.codeanalyzesystem.service.ProjectService;
import com.hlc.codeanalyzesystem.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@RestController
@RequestMapping("/classStructure")
@Slf4j
@CrossOrigin
public class ClassStructureAnaController {

    @Autowired
    private ProjectRevEngineeringService projectRevEngineeringService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private RecordService recordService;

    //响应分析类图
    @RequestMapping("/analysis/{pid}")
    public ResultJSON cdAnalysis(@PathVariable("pid") Integer pid, @RequestParam("userId") Integer userId){
        try{
            String projectName=projectService.queryProjectNameByPid(pid);
            ClassDiagramVo cd = projectRevEngineeringService.cdAnalysis2(pid,projectName);
            Projectgraph projectgraph = new Projectgraph();
            projectgraph.setPid(pid);
            projectgraph.setState(1);
            projectgraph.setType(3);
            /*recordService.insertRecord(userId,pid,projectName,"CDAnalyze","/reengineering");*/
            return new ResultJSON(1,"类结构分析成功",cd);
        }
        catch (Exception e){
            e.printStackTrace();
            log.info("cdAnalysis exception  " + pid);
            return new ResultJSON(-1,e.getMessage(),null);
        }
    }

    //导出类图
    @RequestMapping("/export/{pid}")
    public int exportCD(@PathVariable("pid") Integer pid,@RequestParam("userId") Integer userId, HttpServletResponse response){
        try {
            if (pid != null) {
                String projectName=projectService.queryProjectNameByPid(pid);
                String exports = JSON.toJSONString(projectRevEngineeringService.cdAnalysis2(pid,projectName));
                //System.out.println(exports);
                // 以流的形式下载文件
                InputStream in = new BufferedInputStream(new ByteArrayInputStream(exports.getBytes()));
                byte[] buffer = new byte[in.available()];
                in.read(buffer);
                in.close();
                String downloadFileName = System.currentTimeMillis()+".json";
                response.reset();//清除response中的缓存信息
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.addHeader("Content-Disposition", "attachment;fileName=" + downloadFileName);// 设置文件名
                try {
                    OutputStream os = new BufferedOutputStream(response.getOutputStream());
                    os.write(buffer);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    log.info("cd export exception");
                    return -1;
                }
            }
            return 0;
        } catch (Exception e) {
            log.info("cd export exception");
            return -1;
        }
    }
}
