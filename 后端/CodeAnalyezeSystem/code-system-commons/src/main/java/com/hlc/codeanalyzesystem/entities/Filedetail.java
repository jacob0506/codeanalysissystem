package com.hlc.codeanalyzesystem.entities;


public class Filedetail {

  private Integer id;
  private String fileralativepath;
  private Integer state;
  private Integer pid;
  private Integer lineCount;
  private Integer classCount;
  private Integer functionCount;
  private Integer variableCount;
  private String longestClass;
  private String longestFunction;
  private String classList;
  private String functionList;
  private String variableList;
  private String functionVariable;
  private String fileHalstead;

  public String getFileHalstead() {
    return fileHalstead;
  }

  public void setFileHalstead(String fileHalstead) {
    this.fileHalstead = fileHalstead;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getFileralativepath() {
    return fileralativepath;
  }

  public void setFileralativepath(String fileralativepath) {
    this.fileralativepath = fileralativepath;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Integer getPid() {
    return pid;
  }

  public void setPid(Integer pid) {
    this.pid = pid;
  }


  public Integer getLineCount() {
    return lineCount;
  }

  public void setLineCount(Integer lineCount) {
    this.lineCount = lineCount;
  }


  public Integer getClassCount() {
    return classCount;
  }

  public void setClassCount(Integer classCount) {
    this.classCount = classCount;
  }


  public Integer getFunctionCount() {
    return functionCount;
  }

  public void setFunctionCount(Integer functionCount) {
    this.functionCount = functionCount;
  }


  public Integer getVariableCount() {
    return variableCount;
  }

  public void setVariableCount(Integer variableCount) {
    this.variableCount = variableCount;
  }


  public String getLongestClass() {
    return longestClass;
  }

  public void setLongestClass(String longestClass) {
    this.longestClass = longestClass;
  }


  public String getLongestFunction() {
    return longestFunction;
  }

  public void setLongestFunction(String longestFunction) {
    this.longestFunction = longestFunction;
  }


  public String getClassList() {
    return classList;
  }

  public void setClassList(String classList) {
    this.classList = classList;
  }


  public String getFunctionList() {
    return functionList;
  }

  public void setFunctionList(String functionList) {
    this.functionList = functionList;
  }


  public String getVariableList() {
    return variableList;
  }

  public void setVariableList(String variableList) {
    this.variableList = variableList;
  }


  public String getFunctionVariable() {
    return functionVariable;
  }

  public void setFunctionVariable(String functionVariable) {
    this.functionVariable = functionVariable;
  }

}
