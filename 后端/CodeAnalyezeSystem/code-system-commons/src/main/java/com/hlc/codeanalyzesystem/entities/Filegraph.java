package com.hlc.codeanalyzesystem.entities;


public class Filegraph {

  private Integer id;
  private Integer fid;
  private Integer state;
  private Integer type;
  private String graphjsonpath;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Integer getFid() {
    return fid;
  }

  public void setFid(Integer fid) {
    this.fid = fid;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }


  public String getGraphjsonpath() {
    return graphjsonpath;
  }

  public void setGraphjsonpath(String graphjsonpath) {
    this.graphjsonpath = graphjsonpath;
  }

}
