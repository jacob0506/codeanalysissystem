package com.hlc.codeanalyzesystem.entities;


import java.util.Date;

public class Localproject {

  private Integer id;
  private Integer pid;
  private String path;
  private Date savedtime;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Integer getPid() {
    return pid;
  }

  public void setPid(Integer pid) {
    this.pid = pid;
  }


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }


  public Date getSavedtime() {
    return savedtime;
  }

  public void setSavedtime(Date savedtime) {
    this.savedtime = savedtime;
  }

}
