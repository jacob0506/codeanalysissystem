package com.hlc.codeanalyzesystem.entities;


public class Projectgraph {

  private Integer id;
  private Integer pid;
  private Integer state;
  private Integer type;
  private String graphjsonpath;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Integer getPid() {
    return pid;
  }

  public void setPid(Integer pid) {
    this.pid = pid;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }


  public String getGraphjsonpath() {
    return graphjsonpath;
  }

  public void setGraphjsonpath(String graphjsonpath) {
    this.graphjsonpath = graphjsonpath;
  }

}
