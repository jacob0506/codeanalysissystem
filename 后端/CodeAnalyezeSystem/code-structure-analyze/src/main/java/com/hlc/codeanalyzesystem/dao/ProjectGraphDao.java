package com.hlc.codeanalyzesystem.dao;

import com.hlc.codeanalyzesystem.entities.Projectgraph;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ProjectGraphDao {

   int insertSelective(Projectgraph projectgraph);

   int updateByIdAndJsonPath(@Param("id") Integer id, @Param("jsonPath") String jsonPath);

   Projectgraph selectByPidAndType(@Param("pid") Integer pid, @Param("type") Integer type);

   int deleteByPid(Integer pid);
}
