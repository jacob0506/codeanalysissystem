package com.hlc.codeanalyzesystem.service;

import com.hlc.codeanalyzesystem.dao.ProjectGraphDao;
import com.hlc.codeanalyzesystem.entities.Projectgraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectGraphService {

    @Autowired
    ProjectGraphDao projectGraphDao;

    public int insertProjectGraph(Projectgraph projectgraph){
        return projectGraphDao.insertSelective(projectgraph);
    }
}
