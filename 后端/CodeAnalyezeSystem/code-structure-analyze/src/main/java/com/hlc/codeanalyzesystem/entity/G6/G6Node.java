package com.hlc.codeanalyzesystem.entity.G6;

public class G6Node {
    String label;
    String key;


    public G6Node(String key, String label) {
        this.key = key;
        this.label = label;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
