package com.hlc.codeanalyzesystem.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hlc.codeanalyzesystem.JDT.JDTAnalyze;
import com.hlc.codeanalyzesystem.config.Path;
import com.hlc.codeanalyzesystem.dao.ProjectDao;
import com.hlc.codeanalyzesystem.dao.ProjectGraphDao;
import com.hlc.codeanalyzesystem.dao.ProjectresultDao;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entities.Projectgraph;
import com.hlc.codeanalyzesystem.entity.FileDirNodeVo;
import com.hlc.codeanalyzesystem.entity.G6.G6Graph;
import com.hlc.codeanalyzesystem.entity.Graph;
import com.hlc.codeanalyzesystem.entity.ProjectResultVo;
import com.hlc.codeanalyzesystem.entity.ProjectresultWithBLOBs;
import com.hlc.codeanalyzesystem.util.FileUtil;
import com.hlc.codeanalyzesystem.util.TransformUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectAnalyzeService {

    @Autowired
    private ProjectresultDao projectresultDao;

    @Autowired
    private JDTAnalyze jdtAnalyze;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ProjectGraphDao projectGraphDao;


    public Project selectById(Integer id){
        return projectDao.selectByPrimaryKey(id);
    }

    public ProjectResultVo projectInitialAnalyze(Integer id) throws Exception {
        ProjectresultWithBLOBs projectresultWithBLOBs = projectresultDao.selectByPrimaryKey(id);
        /*Project project = projectDao.selectByPrimaryKey(id);
        if(project.getState()==2){

        }*/
        ProjectResultVo projectResultVo;
        if(projectresultWithBLOBs != null)
        {
            projectResultVo = TransformUtil.transformProjectResultDoToVo(projectresultWithBLOBs);
        }
        else
        {
            projectresultWithBLOBs = new ProjectresultWithBLOBs();
            projectresultWithBLOBs.setId(id);
            jdtAnalyze.projectInitialAnalyze(id,projectresultWithBLOBs);
            projectresultDao.insertSelective(projectresultWithBLOBs);
            projectResultVo = TransformUtil.transformProjectResultDoToVo(projectresultWithBLOBs);
        }
        return projectResultVo;
    }

    public Project projectAnalyze(Integer id) throws Exception {
        /*ProjectresultWithBLOBs projectresultWithBLOBs = projectresultDao.selectByPrimaryKey(id);*/
        Project project = projectDao.selectByPrimaryKey(id);
        if(project.getState()==2){
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            if(fileDirNodeVos.get(0).getDetail()==-1) {
                fileDirNodeVos.get(0).setDetail(project.getId());
                project.setProjectdirjsonpath(JSON.toJSONString(fileDirNodeVos));
                projectDao.updateByPrimaryKeySelective(project);
            }
            return project;
        }
        else
        {
            /*projectresultWithBLOBs = new ProjectresultWithBLOBs();
            projectresultWithBLOBs.setId(id);*/
            jdtAnalyze.projectAnalyze(id,project);
            /*projectresultDao.insertSelective(projectresultWithBLOBs);*/
            project = TransformUtil.transformProject(project);
            project.setState(2);
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            fileDirNodeVos.get(0).setDetail(project.getId());
            project.setProjectdirjsonpath(JSON.toJSONString(fileDirNodeVos));
            projectDao.updateByPrimaryKeySelective(project);
        }
        return project;
    }

    public G6Graph dependencyGraph(Integer id) throws IOException {
        /*ProjectresultWithBLOBs projectresultWithBLOBs = projectresultDao.selectByPrimaryKey(id);*/
        Project project = projectDao.selectByPrimaryKey(id);
        Projectgraph projectgraph = projectGraphDao.selectByPidAndType(id,2);
        if(projectgraph==null) {
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            Integer[] t = fileDirNodeVos.get(0).getResult();
            t[1]=1;
            fileDirNodeVos.get(0).setResult(t);
            project.setProjectdirjsonpath(JSON.toJSONString(fileDirNodeVos));
            projectDao.updateByPrimaryKeySelective(project);
            Graph graph = JSON.parseObject(project.getDependency(),Graph.class);
            G6Graph g6Graph = TransformUtil.transformToG6(graph);
            Projectgraph temp = new Projectgraph();
            temp.setPid(id);
            temp.setType(2);
            temp.setState(2);
            projectGraphDao.insertSelective(temp);
            if (!FileUtil.isDir(Path.Project_dir + id + File.separator + "projectgraph" + File.separator)) {
                FileUtil.makeDirs(Path.Project_dir + id + File.separator + "projectgraph" + File.separator);
            }
            String dir = Path.Project_dir + id + File.separator + "projectgraph" + File.separator + temp.getId() + ".json";
            File file = new File(dir);
            if (!file.exists()) {
                file.createNewFile();
            }
            try {
                FileUtils.write(file, JSON.toJSONString(g6Graph), "utf-8", false);
            } catch (Exception e) {
                System.out.println("error");
            }
            projectGraphDao.updateByIdAndJsonPath(temp.getId(), dir);
            return g6Graph;
        }
        else {
            if(projectgraph.getState()==2){
                System.out.println("从数据库提取");
                String cfgGraphJson;
                File file = new File(projectgraph.getGraphjsonpath());
                cfgGraphJson = FileUtils.readFileToString(file, "UTF-8");
                return JSON.parseObject(cfgGraphJson,G6Graph.class);
            }
            else{
                Graph graph = JSON.parseObject(project.getDependency(),Graph.class);
                G6Graph g6Graph = TransformUtil.transformToG6(graph);
                if(!FileUtil.isDir(Path.Project_dir + id + File.separator + "filegraph" + File.separator)){
                    FileUtil.makeDirs(Path.Project_dir + id + File.separator + "filegraph" + File.separator);
                }
                String dir = Path.Project_dir + id + File.separator + "filegraph" + File.separator + projectgraph.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(g6Graph), "utf-8", false);
                }catch (Exception e){

                }
                projectGraphDao.updateByIdAndJsonPath(projectgraph.getId(),dir);
                return g6Graph;
            }
        }
    }


    public G6Graph callGraph(Integer id) throws IOException {
        /*ProjectresultWithBLOBs projectresultWithBLOBs = projectresultDao.selectByPrimaryKey(id);*/
        Project project = projectDao.selectByPrimaryKey(id);
        Projectgraph projectgraph = projectGraphDao.selectByPidAndType(id,1);
        if(projectgraph==null) {
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            Integer[] t = fileDirNodeVos.get(0).getResult();
            t[0]=1;
            fileDirNodeVos.get(0).setResult(t);
            project.setProjectdirjsonpath(JSON.toJSONString(fileDirNodeVos));
            projectDao.updateByPrimaryKeySelective(project);
            Graph graph = JSON.parseObject(project.getFilecallrelation(),Graph.class);
            G6Graph g6Graph = TransformUtil.transformToG6(graph);
            Projectgraph temp = new Projectgraph();
            temp.setPid(id);
            temp.setType(1);
            temp.setState(2);
            projectGraphDao.insertSelective(temp);
            if (!FileUtil.isDir(Path.Project_dir + id + File.separator + "projectgraph" + File.separator)) {
                FileUtil.makeDirs(Path.Project_dir + id + File.separator + "projectgraph" + File.separator);
            }
            String dir = Path.Project_dir + id + File.separator + "projectgraph" + File.separator + temp.getId() + ".json";
            File file = new File(dir);
            if (!file.exists()) {
                file.createNewFile();
            }
            try {
                FileUtils.write(file, JSON.toJSONString(g6Graph), "utf-8", false);
            } catch (Exception e) {
                System.out.println("error");
            }
            projectGraphDao.updateByIdAndJsonPath(temp.getId(), dir);
            return g6Graph;
        }
        else {
            if(projectgraph.getState()==2){
                System.out.println("从数据库提取");
                String GraphJson;
                File file = new File(projectgraph.getGraphjsonpath());
                GraphJson = FileUtils.readFileToString(file, "UTF-8");
                return JSON.parseObject(GraphJson,G6Graph.class);
            }
            else{
                Graph graph = JSON.parseObject(project.getDependency(),Graph.class);
                G6Graph g6Graph = TransformUtil.transformToG6(graph);
                if(!FileUtil.isDir(Path.Project_dir + id + File.separator + "filegraph" + File.separator)){
                    FileUtil.makeDirs(Path.Project_dir + id + File.separator + "filegraph" + File.separator);
                }
                String dir = Path.Project_dir + id + File.separator + "filegraph" + File.separator + projectgraph.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(g6Graph), "utf-8", false);
                }catch (Exception e){

                }
                projectGraphDao.updateByIdAndJsonPath(projectgraph.getId(),dir);
                return g6Graph;
            }
        }
    }
}
