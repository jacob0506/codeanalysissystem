package com.hlc.codeanalyzesystem.controller;

import com.alibaba.fastjson.JSON;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entities.Projectgraph;
import com.hlc.codeanalyzesystem.entity.*;
import com.hlc.codeanalyzesystem.entity.G6.G6Graph;
import com.hlc.codeanalyzesystem.service.ProjectAnalyzeService;
import com.hlc.codeanalyzesystem.service.ProjectGraphService;
import com.hlc.codeanalyzesystem.service.RecordService;
import com.hlc.codeanalyzesystem.util.TransformUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@RestController
@RequestMapping("/projectAnalyze")
@Slf4j
@CrossOrigin
public class ProjectAnalyzeController {

    @Autowired
    private ProjectAnalyzeService projectAnalyzeService;

    @Autowired
    private RecordService recordService;

    @Autowired
    private ProjectGraphService projectGraphService;

    //项目概况
    @RequestMapping("/initial/{id}")
    public ResultJSON projectInitialAnalyze(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId){
        try {

            Project project = projectAnalyzeService.projectAnalyze(id);
           /* recordService.insertRecord(userId,id,"","ProjectAnalyze","ProjectAnalyze");*/
            System.out.println(project.getProjectdirjsonpath());
            return new ResultJSON(1,"获取目录成功",project);
        }
        catch (Exception e){
            e.printStackTrace();
            log.info("project initial exception" + id + " " + userId);
            return null;
        }
    }

    //项目依赖图
    @RequestMapping("/dependency/{id}")
    public G6Graph projectDependencyGraph(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId) throws IOException {
        G6Graph g6Graph = projectAnalyzeService.dependencyGraph(id);
        return g6Graph;
    }

    //项目调用图
    @RequestMapping("/call/{id}")
    public G6Graph projectCallGraph(@PathVariable("id") Integer id,@RequestParam("userId") Integer userId) throws IOException {
        G6Graph g6Graph = projectAnalyzeService.callGraph(id);
        return g6Graph;
    }

    //导出项目依赖图
    @RequestMapping("/dependency/export/{id}")
    public void projectDependencyGraphExport(@PathVariable("id") Integer id, @RequestParam("userId") Integer userId, HttpServletResponse response){
        try {
            if (id != null) {
                Project project = projectAnalyzeService.selectById(id);
                Graph graph = JSON.parseObject(project.getDependency(),Graph.class);
                TransformUtil.DeleterPrefixPath(graph,id);
                String version = "version 1.0\n";
                List<String> exports = TransformUtil.transformGraphToList(graph);
                String downloadFileName = System.currentTimeMillis()+".txt";
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + downloadFileName);// 设置文件名
                try {
                    OutputStream os = response.getOutputStream();
                    os.write(version.getBytes());
                    for(String str : exports)
                    {
                        os.write(str.getBytes());
                    }
                } catch (Exception e) {
                    log.info("call export exception");

                }
            }
            //recordService.insertRecord(userId,id,"","ProjectDependencyExport","/projectAnalyze/dependency/export/"+ id + "?" +"userId = " + userId);
        } catch (Exception e) {
            log.info("projectDependencyExport exception");

        }
    }

    //导出项目调用图
    @RequestMapping("/call/export/{id}")
    public void projectCallGraphExport(@PathVariable("id") Integer id,@RequestParam("userId") Integer userId, HttpServletResponse response){
        try {
            if (id != null) {
                Project project = projectAnalyzeService.selectById(id);
                Graph graph = JSON.parseObject(project.getFilecallrelation(),Graph.class);
                TransformUtil.DeleterPrefixPath(graph,id);
                String version = "version 1.0\n";
                List<String> exports = TransformUtil.transformGraphToList(graph);
                String downloadFileName = System.currentTimeMillis()+".txt";
                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + downloadFileName);// 设置文件名
                try {
                    OutputStream os = response.getOutputStream();
                    os.write(version.getBytes());
                    for(String str : exports)
                    {
                        os.write(str.getBytes());
                    }
                } catch (Exception e) {
                    log.info("call export exception");

                }
            }
            //recordService.insertRecord(userId,id,"","ProjectCallExport","/projectAnalyze/call/export/"+ id + "?" +"userId = " + userId);

        } catch (Exception e) {
            log.info("projectCallExport exception");

        }
    }

}
