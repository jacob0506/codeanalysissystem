package com.hlc.codeanalyzesystem.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.hlc.codeanalyzesystem.ComplexityAlgorithm.Halstead.HalsteadMetrics;
import com.hlc.codeanalyzesystem.JDT.FileJDTResultModel;
import com.hlc.codeanalyzesystem.JDT.JDTAnalyze;
import com.hlc.codeanalyzesystem.JDT.JdtAstUtil;
import com.hlc.codeanalyzesystem.config.Path;
import com.hlc.codeanalyzesystem.dao.*;
import com.hlc.codeanalyzesystem.entities.Filedetail;
import com.hlc.codeanalyzesystem.entities.Filegraph;
import com.hlc.codeanalyzesystem.entities.Project;
import com.hlc.codeanalyzesystem.entity.*;
import com.hlc.codeanalyzesystem.util.FileUtil;
import com.hlc.codeanalyzesystem.util.TransformUtil;
import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;

@Service
public class FileAnalyzeService {

    @Autowired
    private ProjectresultDao projectresultDao;

    @Autowired
    private JDTAnalyze jdtAnalyze;

    @Autowired
    private FiledetailDao filedetailDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private FilegraphDao filegraphDao;

    String resourcePath = "";

    public List<Filedetail> selectFiledetailByPidAndFilename(Integer pid,String filename){
        /*FileresultExample fileresultExample = new FileresultExample();
        FileresultExample.Criteria criteria = fileresultExample.createCriteria();
        criteria.andBelongtoEqualTo(pid);
        criteria.andNameEqualTo(filename);*/
        return filedetailDao.selectByPidAndFilename(pid,filename);
    }

    public Filedetail fileInitialAnalyze(Integer pid,String filename) throws Exception {
        List<Filedetail> filedetailList = selectFiledetailByPidAndFilename(pid,filename);
        if(filedetailList == null || filedetailList.size() == 0)
        {
            Project project = projectDao.selectByPrimaryKey(pid);
            String absolutePath = filename;
            FileJDTResultModel fileJDTResultModel = jdtAnalyze.fileJDTAnalyze(absolutePath);
            Filedetail filedetail = TransformUtil.transformFileJdtResultToFileResult(fileJDTResultModel);
            filedetail.setPid(pid);
            filedetail.setFileralativepath(filename);
            filedetail.setState(2);
            int id = filedetailDao.insertSelective(filedetail);
            Map<String, HalsteadMetrics> fileComp = (Map<String, HalsteadMetrics>) JSON.parse(project.getComplexfile());
            /*FileResultVo fileResultVo = TransformUtil.transformFileResultToVo(filedetail);*/
            filedetail.setFileHalstead(JSON.toJSONString(fileComp.get(filename)));
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            List<FileDirNodeVo> treeList = new ArrayList<>();
            for(FileDirNodeVo fileDirNodeVo : fileDirNodeVos){
                if(fileDirNodeVo.getNodePath().equals(filename)){
                    fileDirNodeVo.setDetail(filedetail.getId());
                }
                treeList.add(fileDirNodeVo);
            }
            project.setProjectdirjsonpath(JSON.toJSONString(treeList));
            projectDao.updateByPrimaryKeySelective(project);
            return filedetail;
        }
        else
        {
            if(filedetailList.get(0).getState()==1){
                String absolutePath = filename;
                FileJDTResultModel fileJDTResultModel = jdtAnalyze.fileJDTAnalyze(absolutePath);
                Filedetail filedetail = TransformUtil.transformFileJdtResultToFileResult(fileJDTResultModel);
                filedetail.setPid(pid);
                filedetail.setFileralativepath(filename);
                filedetail.setState(2);
                filedetail.setId(filedetailList.get(0).getId());
                filedetailDao.updateByExample(filedetail);
                return filedetail;
            }
            else {
                Project project = projectDao.selectByPrimaryKey(pid);
                Map<String, HalsteadMetrics> fileComp = (Map<String, HalsteadMetrics>) JSON.parse(project.getComplexfile());
                Filedetail filedetail = filedetailList.get(0);
                filedetail.setFileHalstead(JSON.toJSONString(fileComp.get(filename)));
                return filedetail;
            }
        }

    }

    public FileDependencyVo fileDependency(Integer pid,String filename) throws Exception{
        Project project = projectDao.selectByPrimaryKey(pid);
        Graph graph = JSON.parseObject(project.getDependency(),Graph.class);
        Map<Integer,String> idNameMap = graph.getIdNameMapping();
        int [][]g = graph.getGraph();
        int pos = 0;
        for (Map.Entry<Integer,String> entry : idNameMap.entrySet())
        {
            if (filename.equals(entry.getValue()))
            {
                pos = entry.getKey();
            }
        }
        int n = g.length;
        FileDependencyVo fileDependencyVo = new FileDependencyVo();
        for(int i=0;i<n;i++)
        {
            if(g[i][pos] == 1)
            {
                String fullPath = idNameMap.get(i);
                int index = fullPath.indexOf(pid + File.separator);
                fileDependencyVo.getDependency().add(fullPath.substring(index));
            }
        }
        for(int i=0;i<n;i++)
        {
            if(g[pos][i] == 1)
            {
                String fullPath = idNameMap.get(i);
                int index = fullPath.indexOf(pid + File.separator);
                fileDependencyVo.getBeDependentOn().add(fullPath.substring(index));
            }
        }

        return fileDependencyVo;
    }

    public FileDependencyVo fileCall(Integer pid,String filename) throws Exception {
        Project project = projectDao.selectByPrimaryKey(pid);
        Graph graph = JSON.parseObject(project.getFilecallrelation(),Graph.class);
        Map<Integer,String> idNameMap = graph.getIdNameMapping();
        int [][]g = graph.getGraph();
        int pos = 0;
        for (Map.Entry<Integer,String> entry : idNameMap.entrySet())
        {
            if (filename.equals(entry.getValue()))
            {
                pos = entry.getKey();
            }
        }
        int n = g.length;
        FileDependencyVo fileDependencyVo = new FileDependencyVo();
        for(int i=0;i<n;i++)
        {
            if(g[i][pos] == 1)
            {
                String fullPath = idNameMap.get(i);
                int index = fullPath.indexOf(pid + File.separator);
                fileDependencyVo.getDependency().add(fullPath.substring(index));
            }
        }
        for(int i=0;i<n;i++)
        {
            if(g[pos][i] == 1)
            {
                String fullPath = idNameMap.get(i);
                int index = fullPath.indexOf(pid + File.separator);
                fileDependencyVo.getBeDependentOn().add(fullPath.substring(index));
            }
        }
        return fileDependencyVo;
    }

    public List<TreeNodeVo> fileAst(Integer pid,String filename) throws Exception {
        List<Filedetail> filedetails = filedetailDao.selectByPidAndFilename(pid,filename);
        Filegraph filegraph = filegraphDao.selectByFidAndType(filedetails.get(0).getId(),1);
        if(filegraph==null) {
            Project project = projectDao.selectByPrimaryKey(pid);
            List<FileDirNodeVo> treeList = new ArrayList<>();
            List<FileDirNodeVo> fileDirNodeVos = JSON.parseObject(project.getProjectdirjsonpath(),new TypeReference<List<FileDirNodeVo>>(){});
            for(FileDirNodeVo fileDirNodeVo : fileDirNodeVos){
                if(fileDirNodeVo.getNodePath().equals(filename)){
                    Integer[] t = fileDirNodeVo.getResult();
                    t[0]=1;
                    fileDirNodeVo.setResult(t);
                }
                treeList.add(fileDirNodeVo);
            }
            project.setProjectdirjsonpath(JSON.toJSONString(treeList));
            projectDao.updateByPrimaryKeySelective(project);
            String absolutePath = filename;
            CompilationUnit comp = JdtAstUtil.getCompilationUnit(absolutePath);
            List<TreeNodeVo> list = TransformUtil.transformASTNodeToList(comp);
            list.get(0).setChildren(TransformUtil.buildTree(list, 0));
            List<TreeNodeVo> ans = new ArrayList<>();
            ans.add(list.get(0));
            Filegraph temp = new Filegraph();
            temp.setState(2);
            temp.setType(1);
            temp.setFid(filedetails.get(0).getId());
            filegraphDao.insertSelective(temp);
            if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
            }
            String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + temp.getId() +".json";
            File file =new File(dir);
            if(!file.exists()){
                file.createNewFile();
            }
            try{
                FileUtils.write(file, JSON.toJSONString(ans), "utf-8", false);
            }catch (Exception e){
                System.out.println("ddg error");
            }
            filegraphDao.updateByIdAndJsonPath(temp.getId(),dir);
            return ans;
        }
        else{
            if(filegraph.getState()==2){
                System.out.println("从数据库提取");
                String GraphJson;
                File file = new File(filegraph.getGraphjsonpath());
                GraphJson = FileUtils.readFileToString(file, "UTF-8");
                return JSON.parseObject(GraphJson,new TypeReference<List<TreeNodeVo>>(){});
            }
            else{
                String absolutePath = filename;
                CompilationUnit comp = JdtAstUtil.getCompilationUnit(absolutePath);
                List<TreeNodeVo> list = TransformUtil.transformASTNodeToList(comp);
                list.get(0).setChildren(TransformUtil.buildTree(list, 0));
                List<TreeNodeVo> ans = new ArrayList<>();
                ans.add(list.get(0));
                if(!FileUtil.isDir(Path.Project_dir + pid + File.separator + "filegraph" + File.separator)){
                    FileUtil.makeDirs(Path.Project_dir + pid + File.separator + "filegraph" + File.separator);
                }
                String dir = Path.Project_dir + pid + File.separator + "filegraph" + File.separator + filegraph.getId() +".json";
                File file =new File(dir);
                if(!file.exists()){
                    file.createNewFile();
                }
                try{
                    FileUtils.write(file, JSON.toJSONString(ans), "utf-8", false);
                }catch (Exception e){
                    System.out.println("ddg error");
                }
                filegraphDao.updateByIdAndJsonPath(filegraph.getId(),dir);
                return ans;
            }
        }
    }

    public int calculateFileComplexity(Integer pid,String fileName) throws Exception {
        String absolutePath = resourcePath + "/" + pid + "/" + fileName;
        ProjectresultWithBLOBs projectresultWithBLOBs = projectresultDao.selectByPrimaryKey(pid);
        String fileCompJson = projectresultWithBLOBs.getComplexfile();
        Map<String,Integer> map = (Map)JSONObject.parse(fileCompJson);
        return map.getOrDefault(absolutePath,-1);
    }


}
