<h1>code system</h1>

<h2>运行环境：</h2>

java 11 
mysql 8.0.28

<h2>源代码结构</h2>

code-structure-analyze: 代码结构分析模块

code-system-client: 客户端通用模块（负责登录，消费，文件管理功能）

code-system-commons: 包含一些公共的类

code-system-eureka: 注册中心

code-reverser-engineering：代码逆向工程模块



<h2>代码结构分析模块说明：</h2>

controller负责处理所有的请求映射，这里包括ProjectAnalyzeController和FileAnalyzeController，分别处理项目级别和文件级别的分析请求

service负责处理实际的业务逻辑，会先去数据库查询是否有分析结果，如果没有，则调用JDT进行分析

JDT层：提供基于jdt的分析服务，同时这个层会调用ComplexityAlgorithm和Util层。主要的功能位于JDTAnalyze中

dao层：用于操作数据库

entity层：实体层

util层：提供共用功能以及转化类的工具类。

ComplextityAlgorithm层: 复杂度计算层

Analyze层： 最早的分析方法逻辑，有问题，已弃用


<h2>代码结构分析模块类说明：</h2>
核心逻辑就是controller层-> service层 -> dao层  -> JDT层 -> Complexity/ Util层

核心分析算法的实现主要位于JDT层中

JDTAnalyze包含对整个项目进行分析，以及单个文件进行分析的算法

JdtAstutil能够提供抽象语法树根节点

StructureVisitor则是访问节点类，是以观察者模式传入分析的，里面可以定义对于不同节点的提取逻辑

ProjectComplexityUtil中封装了 项目复杂度分析算法

还有一部分复杂度分析算法位于ComplexityAlgorithm中， 其下面的Halstead/HalsteadUtil 封装了对于文件的Halstead复杂度计算方法

主要的工具逻辑位于Util中

TransformUtil中包含了全部对象的转化逻辑，还有建前端树，建前端图等逻辑。

PowFitter 中包含了数学方法对于幂函数的拟合



<h2>部署须根据实际修改路径</h2>
code-structure-common:com.hlc.codeanalyzesystem.config.Path中的Project_dir修改为服务器实际项目存储路径

此外 对于code-structure-analyze 模块

com.hlc.codeanalyzesystem.JDT.JdtAstUtil中第35行为设置环境，建议设置为实际服务器部署java路径，当然也可以去掉该设置，但可能会导致分析结果出现问题，导致对于变量的类型提取结果为unknown

com.hlc.codeanalyzesystem.ComplexityAlgorithm.Halstead.HalsteadUtil中第21行同理
<h2>导出功能说明书</h2>

导出功能包括导出调用图，依赖图，抽象语法树
依赖图格式为dep,调用图格式为call，抽象语法树格式为ast
这三种导出格式中，dep和call其实是同一种格式，只不过便于区分文件类型，分成了两种后缀命名
具体格式见附件

<h2>数据库sql</h2>
见附件