import { request } from "./request";
import qs from "qs";
export function getProjectList(id, pageNo) {
  return request({
    url: "/myProject/list", //获取项目接口
    params: {
      userId: id,
      pageNo: pageNo
    }
  });
}
export function getProjectCount(uid) {
  return request({
    url: "/myProject/listCount", //获取项目页数接口
    params: {
      userId: uid
    }
  });
}
export function uploadProject(formdata,callback) {
  return request({
    url: "/myProject/upload/file", //上传项目接口(文件)
    data: formdata,
    method: "post"
  },callback);
}
export function uploadProjectWithGit(formdata) {
  return request({
    url: "/myProject/upload/gitlab", //上传项目接口(git)
    data: formdata,
    method: "post"
  });
}
export function isProjectExist(name) {
  return request({
    url: "/project/name/exist", //项目名称重复接口
    params: {
      projectName: name
    }
  });
}
export function deleteProject(pid){
  return request({
    url: `/myProject/delete/${pid}`, //删除项目接口
  })
}
export function updateProject(pid,formdata,callback) {
  return request({
    url: `/myProject/update/file`, //更新项目接口(文件)
    data: formdata,
    method: "post"
  },callback);
}
export function updateProjectWithGit(formdata) {
  return request({
    url: "/myProject/update/gitlab", //更新项目接口(git)
    data: formdata,
    method: "post"
  });
}
export function getProjectListSearch(id,name) {
  return request({
    url: "/myProject/SearchList", //获取搜索项目接口
    params: {
      userId: id,
      query: name
    }
  });
}
