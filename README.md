# 代码分析系统

## 目录结构

- 后端
  - CodeAnalyezeSystem：存放后端代码
  - 数据库：存放数据库.sql文件
- 前端
  - AnalysisSystemWeb：存放前端代码
- 系统信息
  - 逆向工程模块信息：模块信息，包括接口信息
  - 用户模块和结构分析模块信息.md：模块信息，包括接口信息和数据库信息

- .gitignore
  - 忽略一些配置文件和本地编译后文件的上传

## git 常用命令

### 初始克隆

```
git clone 从git或gitlab上克隆的地址
```

在运行下列命令时，需要`cd`进仓库的根目录，即带有`.git`文件夹的目录

### 本地添加文件

```
# 查看当前暂存区和本地仓库的状态,此时可以看到被修改的文件，新增的文件等信息，此步可省略
git status

# 添加到暂存区（index），暂存区位于.git文件夹内
git add <filePath>
# 添加所有变化，包括增删改
git add -A 
# 添加增改的文件，不删除本地已删除的文件
git add .

# 查看当前暂存区和本地仓库的状态,此时可以看到位于暂存区的文件，此步可省略
git status

# 提交到本地仓库（Repository），本地仓库位于.git文件夹内
git commit -m "备注"

# 此时查看状态会提示你已经提交了文件，可以使用push命令推送到远程目录，此步可省略
git status

# 推送给远程仓库（Remote），由于只有一个master分支，默认为master分支
git push
```

### 从远程仓库更新本地仓库

```
git pull
```

