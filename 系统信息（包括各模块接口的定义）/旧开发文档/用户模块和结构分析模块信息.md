# 1.需求说明书
##  1.1系统功能需求分析
代码结构分析模块的功能点已于1.3中列举，但功能点之间具有相关性，一个功能点也只是一个大功能的一部分，经过划分，代码结构分析模块可以分为两大类的功能：软件级别功能以及软件内部单个文件级别的功能。用例图如图
![](https://cdn.nlark.com/yuque/__puml/a5d78507a3f4c3fac71505837e365172.svg#crop=0&crop=0&crop=1&crop=1&from=url&id=wRXL2&margin=%5Bobject%20Object%5D&originHeight=323&originWidth=971&originalType=binary&ratio=1&rotation=0&showTitle=false&status=done&style=none&title=)

图1-1 代码结构分析模块用例图

### 1.1.1软件级别功能
#### 1.1.1.1 分析软件概要
   分析软件概要就是指用户选择分析软件后，系统界面上显示软件内的文件数，类数，行数，软件项目中最长的文件，最长的类，最长的方法，以及最复杂的文件，同时显示出软件项目的复杂度，并且在界面左侧显示出软件项目的目录结构，该目录结构可以点击，单击目录展开，单机目录下的文件跳转文件分析。表1-1描述了分析软件概要的用例详情。
表1-1分析软件概要详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已登录并且进入到项目主页 |
| 用例后置条件 | 系统展示软件概要信息 |
| 用例基本事件流 | 1.用户在项目区域中选择其中一个软件
2.用户点击下方的进入分析 
3.系统显示正在分析
4.分析完成后，系统跳转软件分析主页，然后显示软件概要信息 |
| 用例备选事件流 | 4.1分析出现异常，系统显示分析结果为空 |


#### 1.1.1.2 分析软件依赖关系图
   分析软件依赖关系图就是指用户获取软件内文件之间的依赖关系，界面会以思维导图的方式展示出依赖关系，这里所说的依赖关系是指代码文件之间产生了导入关系，即如果文件A中包含了代码 import B,我们则认为A依赖于B。表1-2描述了分析软件依赖的用例详情。

表1-2分析软件依赖关系图详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入软件分析页 |
| 用例后置条件 | 系统显示软件依赖关系图 |
| 用例基本事件流 | 1.用户点击上方获取依赖关系按钮
2.系统显示分析中
3.分析完依赖关系后，向用户展示出整个软件内所有代码文件的依赖关系图 |



#### 1.1.1.3 导出软件依赖关系图
导出软件依赖关系是指将先前分析的软件依赖关系，以dep的文件格式进行导出。dep文件格式将在3.4.2导出文件设计中有详细叙述。表1-3描述了导出软件依赖关系的用例详情。

表1-3分析软件依赖关系图详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入依赖分析页 |
| 用例后置条件 | 系统导出软件依赖关系图 |
| 用例基本事件流 | 1.用户点击页面上的导出按钮
2.系统创建导出任务，在一段时间后，系统导出以.dep为后缀的依赖关系文件 |


#### 1.1.1.4 分析软件调用关系图
   分析软件调用关系图是指分析软件内各文件之间的调用关系，然后以mindmap思维导图的形式展示。这里所说的调用关系是指一个文件对另一个文件中的类进行相关方法的调用，对应抽象语法树中的invocation关系，与依赖关系不同的是，两个类产生import关系后，未必会对另一个方法调用，此外，即使两个类没有产生import关系，但在同一个包下，也可进行方法调用，从而产生调用关系。表1-4描述了分析软件调用的用例详情。
表1-4分析软件调用关系图详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入软件分析页 |
| 用例后置条件 | 系统显示调用关系图 |
| 用例基本事件流 | 1.用户点击上方获取调用关系按钮
2.系统显示分析中
3.分析完调用关系后，向用户展示出整个软件内所有代码文件之间的调用关系图 |

#### 1.1.1.5  导出软件调用关系图
导出软件调用关系是指将先前分析的软件调用关系，以call的文件格式进行导出。call文件格式将在3.4.2导出文件设计中有详细叙述。表1-5描述了导出软件调用的用例详情。
表1-5导出软件调用关系图详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入调用分析页 |
| 用例后置条件 | 系统显示调用关系图 |
| 用例基本事件流 | 1.用户点击页面上的导出按钮
2.系统创建导出任务，在一段时间后，系统导出以.call为后缀的依赖关系文件 |

### 1.1.2文件级别功能
#### 1.1.2.1 分析文件概要
分析文件概要是指用户选择文件后，系统分析出该文件内的所有类，全部变量，函数，函数内的局部变量，再计算出文件的复杂度，函数的复杂度。表1-6描述了分析文件概要的用例详情。
表1-6分析文件概要详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入软件分析界面 |
| 用例后置条件 | 系统显示单个文件的基本信息 |
| 用例基本事件流 | 1.用户点击左侧显示的目录结构中的某一个文件
2.系统跳转文件分析页
3.系统经过分析后，会显示出该文件的概要信息 |
| 用例备选事件流 | 2.1所选文件并非Java代码文件，无法进行分析 |


#### 1.1.2.2 分析文件依赖关系
分析文件依赖关系是指系统分析用户所选择的文件，分析出其依赖于哪些文件以及被哪些文件所依赖。表1-7描述了分析文件依赖关系的用例详情。

表1-7分析文件依赖关系详情

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入软件分析页 |
| 用例后置条件 | 系统显示单个文件依赖关系 |
| 用例基本事件流 | 1.用户点击进入某文件的分析页
2.系统显示正在分析中
3.系统分析完依赖关系后，向用户展示出一个文件的依赖与被依赖关系 |

#### 1.1.2.3 分析文件调用关系
分析文件调用关系是指系统分析用户所选择的文件，分析出其调用了哪些文件以及被哪些文件所调用。表1-8描述了分析文件调用关系的用例详情。
表1-8分析文件调用关系

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入软件分析页 |
| 用例后置条件 | 系统显示文件调用关系 |
| 用例基本事件流 | 1.用户点击进入某文件的分析页
2.系统显示分析中
3.系统分析完调用关系后，向用户展示出一个文件的调用与被调用的关系 |


#### 1.1.2.4 分析文件抽象语法树
分析文件抽象语法树是指系统分析用户所选择的文件，然后构建该文件的抽象语法树，然后在前端展示可点击展开的树结构。表1-9描述了分析文件抽象语法树的用例详情。
表1-9分析文件抽象语法树

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入文件分析页 |
| 用例后置条件 | 系统展示AST |
| 用例基本事件流 | 1.用户点击上方生成AST图按钮
2.系统显示分析中
3.系统后台分析出AST后，向用户展示AST |


#### 1.1.2.5 导出文件抽象语法树
导出文件抽象语法树是指系统分析用户所选择的文件，然后构建该文件的抽象语法树，之后以ast的格式进行导出。ast格式是一个自定义用于描述抽象语法树的格式，里面会以json树的形式来表示树，ast格式详细内容将会在3.4.2中进行详细叙述。表1-10描述了导出文件抽象语法树的用例详情。

表1-10导出文件抽象语法树

| 用例角色 | 普通用户 |
| --- | --- |
| 用例前置条件 | 用户已进入抽象语法树分析页 |
| 用例后置条件 | 系统导出AST |
| 用例基本事件流 | 1.  用户点击页面上的导出按钮
2.  经过一段时间后，系统导出以.ast为后缀的抽象语法树源文件 |


## 1.2非功能性需求
### 1.2.1 安全性
1）系统后端需要保障安全，未登录用户无权对系统进行任何操作。
2）token设置时长为60min，超过时长，token过期需要重新登录

### 1.2.2 性能
1）系统页面访问展示不超过3s，复杂的分析计算操作响应时长不超过20s
2）除分析项目概要接口外，其余接口响应时长不超过10s

### 1.2.3 可维护性
1）项目结构前后端分离，前后端通过网络请求完成交互，尽可能地减少耦合度，从而提高可维护性。
2）代码分析系统包含逆向工程分析模块和代码结构分析模块，代码结构分析系统作为代码分析系统的子模块，对外提供相应分析接口，如果出现相关分析问题，只需要对代码结构分析模块进行排查以及修改，且修改过程中不影响系统内其他模块提供正常服务，易于维护。

### 1.2.4 可拓展性
由于全系统采用微服务框架，如果后续需要添加更多的分析模块，可直接独立开发，最后注册到本系统的注册中心即可，如果需要使用本系统内现有的其他功能，也可直接进行消费，不需要对现有分析模块进行额外更改，即可完成拓展需求。



# 2.源代码
后端github： [https://github.com/hlchlc908/CodeSystem](https://github.com/hlchlc908/CodeSystem)
前端github:    [https://github.com/Reeseee/AnalysisSystemWeb](https://github.com/Reeseee/AnalysisSystemWeb)

# 3.接口设计
接口采用Restful风格设计,客户端与服务端之间的请求是无状态的。每个接口设置唯一的URL路径，请求方法使用的是HTTP所定义的标准方法(GET、POST、PUT等)。
这里仅对代码结构分析模块接口，通用功能接口进行介绍，关于逆向工程模块的接口请参考逆向工程模块资料

### 3.1 软件级别分析接口
#### 3.1.1 分析软件概况
接口URL：/projectAnalyze/initial/{id}
请求方法: GET
传入参数及返回参数见表3-1：
表3-1 分析软件概况接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| id | Integer | 项目Id |
| 返回参数 |  |  |
| id | Integer | 项目id  |
| linecount | Integer | 行数 |
| filecount | Integer | 文件数 |
| classcount | Integer | 类数 |
| functioncount | Integer | 函数数 |
| longestfile | String | 最长的文件 |
| longestclass | String | 最长的类 |
| longestfunction | String | 最长的函数 |
| mostComplexFile | String | 最复杂的文件 |
| projectdir | String | 文件目录结构的Json树形结构 |
| projectComplexity | String | 项目复杂度 |



#### 3.1.2 分析软件依赖图
接口URL：/projectAnalyze/dependency/{id}
请求方法: GET
传入参数及返回参数见表3-2：
表3-2 分析软件依赖接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| id | Integer | 项目Id |
| 返回参数 |  |  |
| edges |      List<G6Edge> | G6格式图的边集合 |
| nodes | List<G6Node> | G6格式图的点集合 |

其中，G6Edge由两个属性组成,分别是起始点和终点的id，均是String类型。G6Node由两个属性组成,分别是点的id和点的标签label，这两个值也均是String类型。

#### 3.1.3 分析软件调用图
接口URL：/projectAnalyze/call/{id}
请求方法: GET
传入参数及返回参数见表3-3：
表3-3 分析软件调用接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| id | Integer | 项目Id |
| 返回参数 |  |  |
| edges | List<G6Edge> | G6格式图的边集合 |
| nodes | List<G6Node> | G6格式图的点集合 |

其中，G6Edge由两个属性组成,分别是起始点和终点的id，均是String类型。G6Node由两个属性组成,分别是点的id和点的标签label，这两个值也均是String类型。



#### 3.1.4 导出软件依赖图
接口URL：/projectAnalyze/dependency/export/{id}
请求方法: GET
传入参数及返回参数见表3-4：
表3-4 导出软件依赖接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| id | Integer | 项目Id |
| 返回参数 |  |  |
| result | Blob | 文件信息流 |



#### 3.1.5 导出软件调用图
接口URL：/projectAnalyze/call/export/{id}
请求方法: GET
传入参数及返回参数见表3-5：
表3-5 导出软件调用接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| id | Integer | 项目Id |
| 返回参数 |  |  |
| result | Blob | 文件信息流 |






### 3.2 文件级别分析接口
#### 3.2.1 分析文件概要
接口URL：/fileAnalyze/initial/{pid}
请求方法: GET
传入参数及返回参数见表3-6：



表3-6 分析文件概要接口传入参数及返回参数及返回参数

| 参数名 | 参数类型 | 参数描述 |  |
| --- | --- | --- | --- |
|    传入参数 |  |  |  |
| userId | Integer | 用户Id |  |
| pid | Integer | 项目Id |  |
| fileName | String | 文件名 |  |
| 返回参数 |  |  |  |
| id | Integer | 主键自增id |  |
| name | String | 文件名 |  |
| belongTo | Integer | 项目id |  |
| lineCount | Integer | 文件行数 |  |
| classCount | Integer | 文件类数 |  |
| functionCount | Integer | 文件方法数 |  |
| variableCount | Integer | 文件全局变量数 |  |
| longestClass | String | 最长的类 |  |
| longestFunction | String | 最长的函数 |  |
| classList | String | 类列表Json |  |
| functionList | String | 方法列表Json |  |
| variableList | String | 变量列表Json |  |
| functionVariable | String | 方法内局部变量列表及方法复杂度Json |  |
| fileHalstead | String | 文件复杂度 |  |


#### 3.2.2 分析文件依赖
接口URL：/fileAnalyze/ dependency/{pid}
请求方法: GET
传入参数及返回参数见表3-7：
表3-7 分析文件依赖接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| pid | Integer | 项目Id |
| fileName | String | 文件名 |
| 返回参数 |  |  |
| name | String | 文件名 |
| dependency | Set<String> | 依赖于哪些文件 |
| beDependentOn | Set<String> | 被哪些文件依赖 |



#### 3.2.3 分析文件调用
接口URL：/fileAnalyze/call/{pid}
请求方法: GET
传入参数及返回参数见表3-8：
表3-8 分析文件调用接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| pid | Integer | 项目Id |
| fileName | String | 文件名 |
| 返回参数 |  |  |
| name | String | 文件名 |
| call | Set<String> | 调用于哪些文件 |
| beCalledOn | Set<String> | 被哪些文件调用 |




#### 3.2.4 分析文件抽象语法树
接口URL：/fileAnalyze/ast/{pid}
请求方法: GET
传入参数及返回参数见表3-9：
表3-9 分析文件抽象语法树接口传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 | 
 |
| --- | --- | --- | --- |
| 传入参数 |  |  | 
 |
| userId | Integer | 用户Id | 
 |
| pid | Integer | 项目Id | 
 |
| fileName | String | 文件名 | 
 |
| 返回参数 |  |  | 
 |
| ast | String | Ast的json树格式 |  |




#### 3.2.5 导出文件抽象语法树
接口URL：/fileAnalyze/ ast/export/{pid}
请求方法: GET
传入参数及返回参数见表3-10：
表3-10 导出文件抽象语法树接口传入参数及返回参数

| 参数名 | 参数类型 |  | 参数描述 |  | 
 |
| --- | --- | --- | --- | --- | --- |
| 传入参数 |  |  |  |  | 
 |
| userId | Integer |  | 用户Id |  | 
 |
| pid | Integer |  | 项目Id |  | 
 |
| fileName | String |  | 文件名 |  | 
 |
| 返回参数 |  |  |  |  | 
 |
| result |  | Blob |  | 文件信息流 |  |



### 3.3 通用功能接口(非结构分析模块)
#### 3.3.1.用户注册
接口路径：/register
请求方法: GET
传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| username | String | 用户名 |
| password | String | 密码 |
| 返回参数 |  |  |
| res | Integer | 0代表成功,-1代表失败 |


#### 3.3.2.用户登录
接口路径：/login
请求方法: POST
传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| username | String | 用户名 |
| password | String | 密码 |
| 返回参数 |  |  |
| code | Integer | 状态码，1000代表成功 |
| msg | String | token |
| Result | id | 用户Id |
|  | username | 用户名 |


#### 3.3.3.用户上传项目
接口路径：/project/upload
请求方法:  POST
传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| projectDir | MultipartFile[] | 项目源代码文件夹 |
| projectName | String | 项目名 |
| userId | Integer | 数据库中唯一标识用户的Id |
| 返回参数 |  |  |
| res | Integer | 0代表成功,-1代表失败 |


#### 3.3.4.查询项目
接口路径：/myProject/list
请求方法:  GET
传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| pageNo | Integer | 页数 |
| 返回参数 |  |  |
| projects | List<Project> | 项目列表 |


#### 3.3.5.查询项目页数
接口路径：/myProject/listCount
请求方法:  GET
传入参数及返回参数

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| 返回参数 |  |  |
| pagecount | Integer | 页数 |


#### 3.3.6用户记录查询
接口路径：/record/list
请求方法:  GET
传入参数及返回参数：

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| pageNo | Integer | 记录页码 |
| 传出参数 |  |  |
| userId | Integer | 用户Id |
| projectId | Integer | 项目Id |
| operation | String | 操作名 |
| operatedate | Date | 操作日期 |
| operateResult | String | 操作结果的url,这个字段会由前端进行拼接处理 |
| filename | String | 文件名 |


#### 3.3.7用户记录页数查询
接口路径：/record/pageCount
请求方法:  GET
传入参数及返回参数：

| 参数名 | 参数类型 | 参数描述 |
| --- | --- | --- |
| 传入参数 |  |  |
| userId | Integer | 用户Id |
| 传出参数 |  |  |
| pageCount | Integer | 页数 |



# 4.数据库设计
代码分析系统采用Mysql数据库作为数据库管理系统，版本为8.0.25，以下数据表字段的数据类型对应于Mysql中的数据类型。此外，数据库脚本中还包含逆向工程的结果记录表，这里不加以叙述，请参见逆向工程模块资料。表4-1记录了用户信息，表4-2记录了所上传软件项目的信息，表4-3是操作记录表，用作分析操作日志，表4-4记录了软件项目的分析结果，表4-5记录了单个文件的分析结果。字段如下列各表所列举。
表4-1Client表(用户表)

| 字段名 | 数据类型 | 含义 |
| --- | --- | --- |
| Id | int | 主键自增id |
| Username | varchar(255) | 用户账号 |
| Password | varchar(255) | 用户密码 |


表4-2 Project表:(软件项目表)

| 字段名 | 数据类型 | 含义 |
| --- | --- | --- |
| Id | int | 主键自增id |
| userId | varchar(255) | 用户id |
| projectName | varchar(255) | 项目名称 |
| UploadDate | datetime | 上传时间 |




表4-3 Record表:记录表

| 字段名 | 数据类型 | 含义 |
| --- | --- | --- |
| Id | int | 主键自增Id |
| userId | int | 与user表中的id对应 |
| projectId | int | 与project表中的id对应 |
| Operation | varchar(255) | 操作名 |
| OperationDate | datetime | 操作时间 |
| OperationResult | varchar(255) | 操作的链接 |
| fileName | varchar(255) | 文件名(可为空) |


表4-4 Projectresult表（项目分析结果表）

| 字段名 | 数据类型 | 含义 |
| --- | --- | --- |
| Id | int | 主键id |
| lineCount | int | 行数 |
| fileCount | int | 文件数 |
| classCount | int | 类数 |
| functionCount | int | 函数数 |
| longestFile | varchar(255) | 最长的文件 |
| longestClass | varchar(255) | 最长的类 |
| projectPackage | longtext | 项目内包 |
| projectDir | longtext | 项目目录 |
| dependency | longtext | 依赖关系 |
| fileCallRelation | longtext | 调用关系 |
| complexFile | longtext | 各文件复杂度 |
| projectComplexity | varchar(255) | 项目复杂度 |


表4-5 FileResult表: 文件分析结果表

| 字段名 | 数据类型 | 含义 |
| --- | --- | --- |
| Id | int | 主键自增id |
| Name | int | 文件名 |
| belongTo | int | 属于哪个项目 |
| lineCount | int | 行数 |
| classCount | int | 类数 |
| functionCount | int | 函数数 |
| variableCount | int | 变量数 |
| longestClass | varchar(255) | 最长的类 |
| longestFunction | varchar(255) | 最长的函数 |
| classList | longtext | 类名列表 |
| functionList | longtext | 函数名列表 |
| variableList | longtext | 变量名列表 |
| functionVariable | longtext | 这里存储函数内部信息（包括函数名，函数内局部变量，函数Halstead复杂度）,对应图3-2中的functionInfo |


数据库脚本
```
/*
 Navicat Premium Data Transfer

 Source Server         : mysqlc
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : codeanalysissystem

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 20/04/2022 10:46:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for fileresult
-- ----------------------------
DROP TABLE IF EXISTS `fileresult`;
CREATE TABLE `fileresult` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `belongTo` int NOT NULL,
  `lineCount` int NOT NULL,
  `classCount` int NOT NULL,
  `functionCount` int NOT NULL,
  `variableCount` int NOT NULL,
  `longestClass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `longestFunction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `classList` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ,
  `functionList` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ,
  `variableList` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ,
  `functionVariable` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `belongTo` (`belongTo`) USING BTREE,
  CONSTRAINT `fileresult_ibfk_1` FOREIGN KEY (`belongTo`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for fileresult_re
-- ----------------------------
DROP TABLE IF EXISTS `fileresult_re`;
CREATE TABLE `fileresult_re` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `belongTo` int NOT NULL,
  `cfg_json` json DEFAULT NULL,
  `ddgWithCtrl_json` json DEFAULT NULL,
  `ddg_json` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `belongTo` (`belongTo`) USING BTREE,
  CONSTRAINT `fileresult_re_ibfk_1` FOREIGN KEY (`belongTo`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `uploadDate` datetime DEFAULT NULL,
  `userId` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `userId` (`userId`) USING BTREE,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for projectresult
-- ----------------------------
DROP TABLE IF EXISTS `projectresult`;
CREATE TABLE `projectresult` (
  `id` int NOT NULL,
  `lineCount` int NOT NULL,
  `fileCount` int NOT NULL,
  `classCount` int NOT NULL,
  `functionCount` int NOT NULL,
  `longestFile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `longestClass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `longestFunction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `projectDir` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `projectPackage` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `dependency` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `fileCallRelation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `complexFile` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `projectComplexity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for projectresult_re
-- ----------------------------
DROP TABLE IF EXISTS `projectresult_re`;
CREATE TABLE `projectresult_re` (
  `id` int NOT NULL,
  `cd_json` json DEFAULT NULL,
  `dirStructure` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int NOT NULL,
  `projectId` int NOT NULL,
  `operation` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `operateDate` datetime DEFAULT NULL,
  `operateResult` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fileName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `userId` (`userId`) USING BTREE,
  KEY `projectId` (`projectId`) USING BTREE,
  CONSTRAINT `record_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `client` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `record_ibfk_2` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1234 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;

```


# 5部署说明
github上已存在代码和jar包
默认linux的文件存取路径为/home/resource，
默认连接个人的数据库
只需要在服务器上执行以下命令便可部署
java -jar code-structure-analyze.jar
java -jar code-system-client.jar
java -jar code-system-eureka.jar

注意：如果在部署时，发生了调用关系无法解析时，需要查找部署服务器的java路径，根据后端代码github上的部署说明中的内容进行修改，如果需要自定义项目源代码存取路径，同样也是根据github上的说明修改相关地方的配置代码 ，如果需要指定数据库配置，请修改相关模块Resouce文件夹下的application.yml。修改完后重新打包。

